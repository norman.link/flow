# Concepts

_flow_ uses [Protocol Buffers](https://developers.google.com/protocol-buffers/) for serialization of data types, [gRPC](https://grpc.io/) for the description of public interfaces and [ReactiveX](http://reactivex.io/) for data flow between nodes.

Protocol Buffers (protobuf) provide an implementation-agnostic data description language to specify data formats. Data types are described in a `.proto` file. From this file, implementations for various programming languages can be generated using the protobuf compiler. The implementations allow serializing and deserializing a data type into a binary data protocol that may be transferred over wire or stored in a file. Protocol buffers are backward and forward compatible and don't need versioning, given the developer adheres to a few simple rules.

gRPC builds on top of the protobuf language to define services. A service specifies rpc calls using protocol buffer types. The gRPC extension to the protobuf compiler then generates code that can be used to implement server and client code for various programming languages.

ReactiveX allows to create transformations on data streams without having to care about source and destination of the data. Data streams are composed of _observables_ that are chained together to create new observables using pre-defined operators. Observables as they are do not do anything, they only create a chain of operations on the data streams. They start to run when a _subscriber_ subscribes to an observable. This also means that a transformation inside an observable is only executed if there is at least one subscriber somewhere down the line that is interested in the data.

For further information please visit the respective websites.

## Node

The main component in _flow_ is called _node_. A node is a logic element for a specific application domain and encapsulates the implementation details. It has a static type _name_ and a dynamic _id_. There can be several instances of the same type. Each instance will be given a unique id for identification. Every node can have a number of input and output slots, as well as properties.

## Slot

A slot has a defined _name_ and protobuf data type. To create an output slot inside a node, you give it a name and pass it an observable of that data type. To create an input slot, you provide the name of the slot and are returned an observable, on which you can do transformations or subscribe. That means, for an identity transformation (that does nothing), you would pass an input slot observable directly to an output slot.

## Property

(not yet implemented)

A node can have a number of properties. Properties are similar to slots in the sense that they can be connected to properties of another node, given they are of the same type. Connected properties are guaranteed to be always in sync to their source properties. Contrary to slots, a property's data type can be either a protobuf _or_ a primitive C++ type, and a property stores it's latest value.

If a property has ingoing _and_ outgoing connections, ingoing property changes will _not_ trigger outgoing connections (NOTE: really?).

## Service

A _service_ is a special type of node that can be accessed from the outside using a gRPC channel. The first step is defining a gRPC service in a `.proto` file. The service node is then implemented by inheriting from this service and implementing the rpc calls. When an instance of the service is created, a client can communicate with that service over a gRPC channel using the provided `.proto` file.

In contrast to nodes, there can only be one instance of a specific service at a time. This is a requirement from the gRPC library.

## Graph

The _graph_ represents the current structure of the application. It contains a set of nodes and services with connections between their slots. In the initialization stage prior to running the graph, instances of nodes are created and their ids are set. During this stage, the nodes create inputs and outputs, usually in their constructors. After initialization, slots are connected as required, and the graph is started. A running graph's structure cannot be changed. Modifications to the graph structure requires that the graph be stopped. Changing properties on a running graph is possible.

## Interface

The graph provides a set of gRPC services. The public binary interface on the library itself is minimal. All access to the graph, e.g. creating nodes, connecting nodes, stopping and starting the graph, needs to be done through the services. If access to the interface is required from the application, it can retrieve the in-process channel to the gRPC server from the graph and use the `.proto` files to call rpc functions.
