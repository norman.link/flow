# Plugin structure

A plugin is a library that contains a set of nodes and services. Public interfaces are defined using `.proto` files. Data types used by nodes are specified using protobuf notation, services are defined using gRPC services.

## `data/`

Contains all relevant data needed for the plugin. This includes, but is not limited to, graph definition files and json configuration files.

## `proto/`

Contains `.proto` files that describe the services and data types used by this plugin. This directory contains the public api for this plugin. Other plugins that depend on this plugin load one or more `.proto` files and generate classes for data types and services.

## `src/`

Contains all the implementation and generated code.

## `test/`

Contains unit tests.

## `test/data/`

Contains test data.

## `test/src/`

Contains test implementations.
