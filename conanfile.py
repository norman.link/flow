from conans import ConanFile, CMake, tools

class PluginConan(ConanFile):
    name = "flow"
    version = "0.0.1"
    license = "TODO"
    url = "https://gitlab.com/flow-framework/plugin.git"
    description = "Base package for flow-based plugins"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    exports = "CMakeLists.txt"
    exports_sources = "cmake/*", "flow/*", "examples/*"
    requires = (("gRPC/1.10.0@thirdparty/stable"),
        ("rxcpp/master@thirdparty/stable"),
        ("jsoncpp/1.8.4@thirdparty/stable"))

    def source(self):
        tools.replace_in_file("CMakeLists.txt", "project(${PACKAGE_NAME} VERSION ${PACKAGE_VERSION} LANGUAGES CXX)", '''project(${PACKAGE_NAME} VERSION ${PACKAGE_VERSION} LANGUAGES CXX)
include(${CMAKE_BINARY_DIR}/../conanbuildinfo.cmake)
conan_basic_setup()''')

    def build(self):
        cmake = CMake(self)
        cmake.configure(build_dir="build")
        cmake.build()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["common"]
