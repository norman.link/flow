#include "generated/property.pb.h"

#include <gtest/gtest.h>
#include <flow/property_container.h>

namespace
{
    class PropertyContainerIntTest
        : public testing::Test
    {
    protected:
        flow::PropertyContainer<int> prop;
        static constexpr char* propName = "TestProperty";

        PropertyContainerIntTest()
            : prop{propName}
        {}

        void SetUp()
        {
            prop = flow::PropertyContainer<int>(propName);
        }
    };

    TEST_F(PropertyContainerIntTest, HasName)
    {
        EXPECT_EQ(prop.name(), propName);
    }

    TEST_F(PropertyContainerIntTest, CanQuery)
    {
        prop.set(42);
        prop.query([](const int& value)
        {
            EXPECT_EQ(value, 42);
        });
    }

    TEST_F(PropertyContainerIntTest, CanMutate)
    {
        prop.mutate([](int& value)
        {
            value = 42;
        });
        EXPECT_EQ(prop.get(), 42);
    }

    TEST_F(PropertyContainerIntTest, HasImplicitDefaultValue)
    {
        auto value = prop.get();
        EXPECT_EQ(value, 0);
    }

    TEST_F(PropertyContainerIntTest, HasExplicitDefaultValue)
    {
        auto prop = flow::PropertyContainer<int>(propName, 42);
        auto value = prop.get();
        EXPECT_EQ(value, 42);
    }

    TEST_F(PropertyContainerIntTest, CanSetGetValue)
    {
        EXPECT_NO_THROW(prop.set(42));
        auto value = prop.get();
        EXPECT_EQ(prop.get(), 42);
    }

    TEST_F(PropertyContainerIntTest, DescriptorValid)
    {
        EXPECT_NE(prop.descriptor(), nullptr);
    }

    TEST_F(PropertyContainerIntTest, ToAnyValid)
    {
        prop.set(42);

        auto anyValue = prop.toAny();

        EXPECT_EQ(anyValue->type_url(), "type.googleapis.com/google.protobuf.Int32Value");

        google::protobuf::Int32Value value;
        anyValue->UnpackTo(&value);
        EXPECT_EQ(value.value(), 42);
    }

    TEST_F(PropertyContainerIntTest, IntFromAnyValid)
    {
        {
            google::protobuf::Int32Value value;
            value.set_value(42);

            google::protobuf::Any anyValue;
            anyValue.PackFrom(value);

            prop.fromAny(anyValue);
        }

        {
            auto value = prop.get();
            EXPECT_EQ(prop.get(), 42);
        }
    }

    TEST_F(PropertyContainerIntTest, ObservableSendsCurrentValue)
    {
        prop.set(42);
        auto subscription = prop.observable().subscribe([](int value)
        {
            EXPECT_EQ(value, 42);
        });
        subscription.unsubscribe();
    }

    TEST_F(PropertyContainerIntTest, ObservableSendsChangedValues)
    {
        bool initialized = false;
        auto subscription = prop.observable().subscribe([&initialized](int value)
        {
            if (!initialized)
            {
                EXPECT_EQ(value, 0);
                initialized = true;
            }
            else
            {
                EXPECT_EQ(value, 42);
            }
        });
        prop.set(42);
        subscription.unsubscribe();
    }
}

namespace
{
    class PropertyContainerProtoTest
        : public testing::Test
    {
    protected:
        flow::PropertyContainer<test::PropertyTestType> prop;
        static constexpr char* propName = "TestProperty";

        PropertyContainerProtoTest()
            : prop{propName}
        {}

        void SetUp()
        {
            prop = flow::PropertyContainer<test::PropertyTestType>(propName);
        }
    };

    TEST_F(PropertyContainerProtoTest, CanQuery)
    {
        test::PropertyTestType value;
        value.set_answer(42);
        value.set_pi(3.14f);

        prop.set(std::move(value));

        prop.query([](const auto& value)
        {
            EXPECT_EQ(value.answer(), 42);
            EXPECT_EQ(value.pi(), 3.14f);
        });
    }

    TEST_F(PropertyContainerProtoTest, CanMutate)
    {
        prop.mutate([](test::PropertyTestType& value)
        {
            value.set_answer(42);
            value.set_pi(3.14f);
        });

        EXPECT_EQ(prop.get().answer(), 42);
        EXPECT_EQ(prop.get().pi(), 3.14f);
    }

    TEST_F(PropertyContainerProtoTest, HasImplicitDefaultValue)
    {
        auto value = prop.get();

        EXPECT_EQ(value.answer(), 0);
        EXPECT_EQ(value.pi(), 0);
    }

    TEST_F(PropertyContainerProtoTest, HasExplicitDefaultValue)
    {
        test::PropertyTestType value;
        value.set_answer(42);
        value.set_pi(3.14f);

        auto prop = flow::PropertyContainer<test::PropertyTestType>(propName, std::move(value));

        {
            auto propertyValue = prop.get();

            EXPECT_EQ(propertyValue.answer(), 42);
            EXPECT_EQ(propertyValue.pi(), 3.14f);
        }
    }

    TEST_F(PropertyContainerProtoTest, CanSetGetValue)
    {
        {
            test::PropertyTestType value;
            value.set_answer(42);
            value.set_pi(3.14f);

            prop.set(std::move(value));
        }

        {
            auto value = prop.get();

            EXPECT_EQ(value.answer(), 42);
            EXPECT_EQ(value.pi(), 3.14f);
        }
    }

    TEST_F(PropertyContainerProtoTest, DescriptorValid)
    {
        EXPECT_NE(prop.descriptor(), nullptr);
    }

    TEST_F(PropertyContainerProtoTest, ToAnyValid)
    {
        {
            test::PropertyTestType value;
            value.set_answer(42);
            value.set_pi(3.14f);

            prop.set(std::move(value));
        }

        {
            auto anyValue = prop.toAny();

            EXPECT_EQ(anyValue->type_url(), "type.googleapis.com/test.PropertyTestType");

            test::PropertyTestType value;
            anyValue->UnpackTo(&value);

            EXPECT_EQ(value.answer(), 42);
            EXPECT_EQ(value.pi(), 3.14f);
        }
    }

    TEST_F(PropertyContainerProtoTest, FromAnyValid)
    {
        {
            test::PropertyTestType value;
            value.set_answer(42);
            value.set_pi(3.14f);

            google::protobuf::Any anyValue;
            anyValue.PackFrom(value);

            prop.fromAny(anyValue);
        }

        {
            auto value = prop.get();

            EXPECT_EQ(value.answer(), 42);
            EXPECT_EQ(value.pi(), 3.14f);
        }
    }
}
