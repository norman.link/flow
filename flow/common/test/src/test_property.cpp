#include "generated/property.pb.h"

#include <gtest/gtest.h>
#include <flow/property.h>

namespace
{
    class PropertyTestInt
        : public testing::Test
    {
    protected:
        flow::Property<int> prop;
        flow::PropertyContainer<int> container;
        static constexpr char* propName = "TestProperty";

        PropertyTestInt()
            : container{propName}
            , prop{container}
        {}
    };

    TEST_F(PropertyTestInt, EmptyPropertyThrows)
    {
        flow::Property<int> prop;
        using Exception = flow::Property<int>::PropertyNotSetException;
        EXPECT_THROW(prop.name(), Exception);
        EXPECT_THROW(prop.query([](const auto&){}), Exception);
        EXPECT_THROW(prop.mutate([](auto&){}), Exception);
        EXPECT_THROW(int value = prop, Exception);
        EXPECT_THROW(int value = prop.value(), Exception);
        EXPECT_THROW(prop = 42, Exception);
        EXPECT_THROW(prop.observable(), Exception);
    }

    TEST_F(PropertyTestInt, HasName)
    {
        EXPECT_EQ(prop.name(), container.name());
    }

    TEST_F(PropertyTestInt, CanQuery)
    {
        prop = 42;
        prop.query([](const int& value)
        {
            EXPECT_EQ(value, 42);
        });
    }

    TEST_F(PropertyTestInt, CanMutate)
    {
        prop.mutate([](int& value)
        {
            value = 42;
        });
        EXPECT_EQ(prop, 42);
    }

    TEST_F(PropertyTestInt, CanGetSetValue)
    {
        prop = 42;
        EXPECT_EQ(prop, 42);
        EXPECT_EQ(prop.value(), 42);
    }
}

namespace
{
    class PropertyTestProto
        : public testing::Test
    {
    protected:
        flow::Property<test::PropertyTestType> prop;
        flow::PropertyContainer<test::PropertyTestType> container;
        static constexpr char* propName = "TestProperty";

        PropertyTestProto()
            : container{propName}
            , prop{container}
        {}
    };

    TEST_F(PropertyTestProto, EmptyPropertyThrows)
    {
        flow::Property<test::PropertyTestType> prop;
        using Exception = flow::Property<test::PropertyTestType>::PropertyNotSetException;
        EXPECT_THROW(prop.name(), Exception);
        EXPECT_THROW(prop.query([](const auto&){}), Exception);
        EXPECT_THROW(prop.mutate([](auto&){}), Exception);
        EXPECT_THROW(test::PropertyTestType value = prop, Exception);
        EXPECT_THROW(test::PropertyTestType value = prop.value(), Exception);
        EXPECT_THROW(prop = test::PropertyTestType(), Exception);
        EXPECT_THROW(prop.observable(), Exception);
    }

    TEST_F(PropertyTestProto, CanQuery)
    {
        test::PropertyTestType value;
        value.set_answer(42);
        value.set_pi(3.14f);
        prop = std::move(value);
        prop.query([](const auto& value)
        {
            EXPECT_EQ(value.answer(), 42);
            EXPECT_EQ(value.pi(), 3.14f);
        });
    }

    TEST_F(PropertyTestProto, CanMutate)
    {
        prop.mutate([](auto& value)
        {
            value.set_answer(42);
            value.set_pi(3.14f);
        });
        test::PropertyTestType value = prop;
        EXPECT_EQ(value.answer(), 42);
        EXPECT_EQ(value.pi(), 3.14f);
    }

    TEST_F(PropertyTestProto, CanGetSetValue)
    {
        {
            test::PropertyTestType value;
            value.set_answer(42);
            value.set_pi(3.14f);
            prop = std::move(value);
        }

        {
            test::PropertyTestType value = prop;
            EXPECT_EQ(value.answer(), 42);
            EXPECT_EQ(value.pi(), 3.14f);
        }

        {
            test::PropertyTestType value = prop.value();
            EXPECT_EQ(value.answer(), 42);
            EXPECT_EQ(value.pi(), 3.14f);
        }
    }
}
