#include <flow/plugin/plugin_interface.h>
#include <flow/concurrency/coordinator.h>

#include <grpc++/impl/grpc_library.h>
#include <google/protobuf/stubs/common.h>

namespace flow::plugin
{
    PluginInterface::PluginInterface()
        : m_factory{std::make_unique<Factory>()}
    {
    }

    PluginInterface::~PluginInterface()
    {
        google::protobuf::ShutdownProtobufLibrary();
    }

    void PluginInterface::init(std::shared_ptr<concurrency::CoordinatorBase> coordinator)
    {
        if (m_coordinator)
        {
            throw std::runtime_error("init cannot be called twice");
        }

        grpc_init();
        m_coordinator = std::dynamic_pointer_cast<concurrency::Coordinator>(coordinator);
    }

    FactoryBase& PluginInterface::factory() const
    {
        return *m_factory;
    }

    concurrency::Coordinator& PluginInterface::coordinator() const
    {
        if (!m_coordinator)
        {
            throw std::runtime_error("coordinator is invalid");
        }

        return *m_coordinator;
    }
}
