#include <flow/slot_base.h>
#include <flow/node_base.h>
#include <flow/concurrency/coordinator.h>
#include <flow/plugin/plugin_interface.h>

namespace flow
{
    SlotBase::SlotBase(
        std::string name,
            const google::protobuf::Descriptor* descriptor,
            const NodeBase& node)
        : m_name{std::move(name)}
        , m_descriptor{descriptor}
        , m_node(node)
    {
    }

    std::string SlotBase::name() const
    {
        return m_name;
    }

    const google::protobuf::Descriptor* SlotBase::descriptor() const
    {
        return m_descriptor;
    }

    concurrency::Coordinator& SlotBase::coordinator() const
    {
        return plugin::PluginInterface::get().coordinator();
    }

    const NodeBase& SlotBase::node() const
    {
        return m_node;
    }

    bool SlotBase::connectableTo(const SlotBase& other) const
    {
        return other.descriptor()->full_name() == m_descriptor->full_name();
    }
}
