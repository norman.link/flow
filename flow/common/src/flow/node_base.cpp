#include <flow/node_base.h>
#include <flow/concurrency/coordinator.h>

namespace flow
{
    NodeBase::NodeBase(std::shared_ptr<const NodeDefinition> definition)
        : m_definition{std::move(definition)}
    {
    }

    const NodeDefinition& NodeBase::definition() const
    {
        return *m_definition;
    }

    std::vector<std::reference_wrapper<InputSlotBase>> NodeBase::inputSlots() const
    {
        std::vector<std::reference_wrapper<InputSlotBase>> inputSlots;
        std::transform(m_inputSlots.begin(), m_inputSlots.end(), std::back_inserter(inputSlots),
            [](const auto& inputSlot) -> std::reference_wrapper<InputSlotBase>
            {
                return *inputSlot;
            }
        );
        return inputSlots;
    }

    std::vector<std::reference_wrapper<OutputSlotBase>> NodeBase::outputSlots() const
    {
        std::vector<std::reference_wrapper<OutputSlotBase>> outputSlots;
        std::transform(m_outputSlots.begin(), m_outputSlots.end(), std::back_inserter(outputSlots),
            [](const auto& outputSlot) -> std::reference_wrapper<OutputSlotBase>
            {
                return *outputSlot;
            }
        );
        return outputSlots;
    }

    std::vector<std::reference_wrapper<PropertyBase>> NodeBase::properties() const
    {
        std::vector<std::reference_wrapper<PropertyBase>> properties;
        std::transform(m_properties.begin(), m_properties.end(), std::back_inserter(properties),
            [](const auto& property) -> std::reference_wrapper<PropertyBase>
            {
                return *property;
            }
        );
        return properties;
    }

    void NodeBase::setId(std::string id)
    {
        m_id = std::move(id);
    }

    std::string NodeBase::id() const
    {
        return m_id;
    }
}
