#include <flow/input_slot.h>
#include <flow/node_base.h>

namespace flow
{
    InputSlotBase::InputSlotBase(
        std::string name,
        const google::protobuf::Descriptor* descriptor,
        const NodeBase& node)
        : SlotBase(std::move(name), descriptor, node)
    {
    }

    bool InputSlotBase::operator==(const InputSlotBase& other) const
    {
        return name() == other.name() &&
            descriptor()->full_name() == other.descriptor()->full_name();
    }
}
