#include <flow/service_base.h>

namespace flow
{
    ServiceBase::ServiceBase(std::shared_ptr<const ServiceDefinition> definition)
        : NodeBase(std::move(definition))
    {
    }

    const ServiceDefinition& ServiceBase::definition() const
    {
        return dynamic_cast<const ServiceDefinition&>(NodeBase::definition());
    }

    void ServiceBase::setId(std::string id)
    {
        std::string serviceFullName = definition().serviceName;
        if (id != serviceFullName)
        {
            throw std::runtime_error("service id \"" + id +
                "\" must match fully qualified service name \"" + serviceFullName + "\"");
        }

        NodeBase::setId(std::move(id));
    }
}
