#include <flow/factory.h>
#include <flow/node_base.h>

#include <algorithm>

namespace flow
{
    std::vector<Factory::NodeId> Factory::list() const
    {
        std::vector<FactoryBase::NodeId> ids;

        for (const auto& factoryFunction : m_factories)
        {
            ids.push_back(factoryFunction.first);
        }

        return ids;
    }

    std::unique_ptr<NodeBase> Factory::make(FactoryBase::NodeId id) const
    {
        auto it = m_factories.find(id);

        if (it == m_factories.end())
        {
            throw std::runtime_error("could not find node \"" + id + "\"");
        }

        return it->second();
    }
}
