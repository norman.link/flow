#include <flow/output_slot.h>
#include <flow/node_base.h>

namespace flow
{
    OutputSlotBase::OutputSlotBase(
        std::string name,
        const google::protobuf::Descriptor* descriptor,
        const NodeBase& node)
        : SlotBase(std::move(name), descriptor, node)
    {
    }

    bool OutputSlotBase::operator==(const OutputSlotBase& other) const
    {
        return name() == other.name() &&
            descriptor()->full_name() == other.descriptor()->full_name();
    }
}
