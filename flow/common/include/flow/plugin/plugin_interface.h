#pragma once

#include <flow/singleton.h>
#include <flow/factory.h>

#include <memory>

namespace grpc
{
    class CoreCodegenInterface;
    class GrpcLibraryInterface;
}

namespace flow::concurrency
{
    class CoordinatorBase;
    class Coordinator;
}

namespace flow::plugin
{
    class SchedulerProvider;

    class __declspec(dllexport) PluginInterfaceBase
    {
    public:
        virtual ~PluginInterfaceBase() = default;

        virtual void init(std::shared_ptr<concurrency::CoordinatorBase> coordinator) = 0;

        virtual FactoryBase& factory() const = 0;

    protected:
        PluginInterfaceBase() = default;
    };

    class PluginInterface
        : public Singleton<PluginInterface>
        , public PluginInterfaceBase
    {
        friend class Singleton<PluginInterface>;

    public:
        void init(std::shared_ptr<concurrency::CoordinatorBase> coordinator) override;

        FactoryBase& factory() const override;
        concurrency::Coordinator& coordinator() const;

    private:
        PluginInterface();
        ~PluginInterface();

        std::unique_ptr<FactoryBase> m_factory;
        std::shared_ptr<concurrency::Coordinator> m_coordinator;
    };
}
