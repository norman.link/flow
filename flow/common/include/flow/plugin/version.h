#pragma once

namespace flow::plugin
{
    // see https://semver.org/
    struct Version
    {
        unsigned major;
        unsigned minor;
        unsigned patch;
    };
}
