#pragma once

#include "version.h"
#include "plugin_interface.h"

#ifdef BUILD_DLL
#define DLLAPI __declspec(dllexport)
#else
#define DLLAPI __declspec(dllimport)
#endif

#define FLOW_PLUGIN(major, minor, revision) \
extern "C" DLLAPI flow::plugin::Version version(void) \
{ \
    return {major, minor, revision}; \
}

extern "C" DLLAPI int build(char** const buf, int& len)
{
    len = 32;
    *buf = new char[len];
    return sprintf_s(*buf, len, "%s %s", __DATE__, __TIME__);
}

extern "C" DLLAPI flow::plugin::PluginInterfaceBase& interface()
{
    return flow::plugin::PluginInterface::get();
}

#ifdef _WIN32
#include <Windows.h>
extern "C" DLLAPI BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    switch (fdwReason)
    {
    case DLL_PROCESS_ATTACH:
        break;
    case DLL_PROCESS_DETACH:
        break;
    case DLL_THREAD_ATTACH:
        break;
    case DLL_THREAD_DETACH:
        break;
    }
    return TRUE;
}
#else
#pragma message("Plugin system is not yet implemented for different platforms")
#endif
