#pragma once

#include <string>
#include <memory>
#include <functional>
#include <unordered_map>

namespace flow
{
    class NodeBase;
    class ServiceBase;

    class __declspec(dllexport) FactoryBase
    {
    public:
        virtual ~FactoryBase() = default;

        using NodeId = std::string;

        virtual std::vector<NodeId> list() const = 0;
        virtual std::unique_ptr<NodeBase> make(NodeId id) const = 0;

    protected:
        FactoryBase() = default;
    };

    class Factory
        : public FactoryBase
    {
    public:
        Factory() = default;

        template <typename T>
        void registerNode()
        {
            NodeId id = getId<T>();

            if (m_factories.count(id))
            {
                throw std::runtime_error("node \"" + id + "\" is already registered");
            }

            m_factories[std::move(id)] =
                []()
                {
                    return std::make_unique<T>();
                };
        }

        std::vector<FactoryBase::NodeId> list() const override;
        std::unique_ptr<NodeBase> make(FactoryBase::NodeId id) const override;

    private:
        using FactoryFunction = std::function<std::unique_ptr<NodeBase>()>;

        template <typename T>
        FactoryBase::NodeId getId() const
        {
            return T::definition().name;
        }

        std::unordered_map<FactoryBase::NodeId, FactoryFunction> m_factories;
    };
}
