#pragma once

#include "property_base.h"
#include "type_traits.h"

#include <rxcpp/rx.hpp>
#include <mutex>
#include <optional>
#include <google/protobuf/util/json_util.h>

namespace rx = rxcpp;

namespace flow
{
    template <typename TValue>
    class PropertyContainer
        : public PropertyBase
    {
    public:
        PropertyContainer(std::string name)
            : PropertyBase(std::move(name))
        {
            TypeTraits<TValue>::default(m_value);
        }

        PropertyContainer(std::string name, TValue&& default)
            : PropertyBase(std::move(name))
        {
            TypeTraits<TValue>::set(default, m_value);
        }

        PropertyContainer(PropertyContainer<TValue>&& other)
        {
            *this = std::move(other);
        }

        PropertyContainer<TValue>& operator=(PropertyContainer<TValue>&& other)
        {
            if (this != &other)
            {
                PropertyBase::operator=(std::move(other));
                m_value = std::move(other.m_value);
                m_subject = std::move(other.m_subject);
            }

            return *this;
        }

        void query(std::function<void(const TValue&)> queryFunc) const
        {
            std::unique_lock<std::mutex> lock(m_mutex);

            decltype(auto) value = TypeTraits<TValue>::get(m_value);
            queryFunc(value);
        }

        void mutate(std::function<void(TValue&)> mutationFunc)
        {
            std::unique_lock<std::mutex> lock(m_mutex);

            decltype(auto) value = TypeTraits<TValue>::get(m_value);
            mutationFunc(value);

            if (!std::is_reference<decltype(value)>())
            {
                TypeTraits<TValue>::set(value, m_value);
            }

            if (m_subject.has_observers())
            {
                // since rx is asynchronous, only copies can be sent
                TValue changedValue = value;
                m_subject.get_subscriber().on_next(std::move(changedValue));
            }
        }

        TValue get() const
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            return TypeTraits<TValue>::get(m_value);
        }

        void set(TValue&& value)
        {
            std::unique_lock<std::mutex> lock(m_mutex);

            typename TypeTraits<TValue>::ProtobufType intermediate;
            TypeTraits<TValue>::set(value, intermediate);
            m_value = std::move(intermediate);

            if (m_subject.has_observers())
            {
                m_subject.get_subscriber().on_next(std::move(value));
            }
        }

        const google::protobuf::FileDescriptor* descriptor() const override
        {
            return TypeTraits<TValue>::ProtobufType::descriptor()->file();
        }

        const google::protobuf::Any* toAny() const override
        {
            auto value = google::protobuf::Any::default_instance().New();
            value->PackFrom(m_value);
            return value;
        }

        void fromAny(const google::protobuf::Any& value) override
        {
            value.UnpackTo(&m_value);
            
            if (m_subject.has_observers())
            {
                m_subject.get_subscriber().on_next(TypeTraits<TValue>::get(m_value));
            }
        }

        rx::observable<TValue> observable() const
        {
            auto initialValue = rx::observable<>::create<TValue>(
                [this](auto& subscriber)
                {
                    {
                        std::unique_lock<std::mutex> lock(m_mutex);
                        subscriber.on_next(TypeTraits<TValue>::get(m_value));
                    }

                    subscriber.on_completed();
                });

            return initialValue.concat(m_subject.get_observable());
        }

    private:
        mutable std::mutex m_mutex;
        typename TypeTraits<TValue>::ProtobufType m_value;
        rx::subjects::subject<TValue> m_subject;
    };
}
