#pragma once

#include "node_base.h"

#include <string>
#include <memory>

namespace flow
{
    template <typename TDerived>
    class Node
        : public NodeBase
    {
    public:
        Node()
            : NodeBase(definitionPtr())
        {}

        static constexpr const NodeDefinition& definition()
        {
            return *definitionPtr();
        }

    private:
        static constexpr std::shared_ptr<const NodeDefinition> definitionPtr()
        {
            if (!m_definition)
            {
                m_definition = std::make_shared<NodeDefinition>();
                m_definition->type = NodeDefinition::Type::Node;
                m_definition->name = name<TDerived>(true);
            }

            return m_definition;
        }

        void setId(std::string id) override final
        {
            NodeBase::setId(std::move(id));
        }

        // will use T::name() if it was implemented
        template <typename T>
        static auto name(bool)
            -> decltype(T::name, std::string{})
        {
            return T::name();
        }

        // will use typeid as a default
        template <typename T>
        static auto name(int)
            -> std::string
        {
            #pragma message ("WARNING: A node has no static name(), will use typeid instead")
            return typeid(T).name();
        }

        static inline std::shared_ptr<NodeDefinition> m_definition{nullptr};
    };
}
