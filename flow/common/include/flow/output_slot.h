#pragma once

#include "slot_base.h"

namespace flow
{
    template <typename TMessage>
    using OutputStream = rx::observable<StreamData<TMessage>>;

    class OutputSlotBase
        : public SlotBase
    {
    public:
        virtual bool operator==(const OutputSlotBase& other) const;

    protected:
        OutputSlotBase(
            std::string name,
            const google::protobuf::Descriptor* descriptor,
            const NodeBase& node);
    };

    template <typename TMessage>
    class OutputSlot
        : public OutputSlotBase
    {
    public:
        OutputSlot(
            std::string name,
            rx::observable<StreamData<TMessage>>&& observable,
            const NodeBase& node)
            : OutputSlotBase(std::move(name), TMessage::descriptor(), node)
        {
            m_observeWorker = coordinator().create_coordinator().get_worker();
            m_subscribeWorker = coordinator().create_coordinator().get_worker();

            m_observable = observable
                .observe_on(rx::serialize_same_worker(m_observeWorker))
                .subscribe_on(rx::serialize_same_worker(m_subscribeWorker))
                .publish()
                .ref_count();
        }

        ~OutputSlot()
        {
            m_observeWorker.unsubscribe();
            m_subscribeWorker.unsubscribe();
        }

        rx::observable<StreamData<TMessage>>& observable()
        {
            return m_observable;
        }

    private:
        rx::observable<StreamData<TMessage>> m_observable;
        rx::rxsc::worker m_observeWorker;
        rx::rxsc::worker m_subscribeWorker;
    };
}
