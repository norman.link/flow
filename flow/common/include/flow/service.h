#pragma once

#include "service_base.h"

namespace flow
{
    template <typename TDerived, typename TGenerated>
    class Service
        : public ServiceBase
        , public TGenerated::Service
    {
    public:
        Service()
            : ServiceBase(definitionPtr())
        {}

        static constexpr const ServiceDefinition& definition()
        {
            return *definitionPtr();
        }

    private:
        grpc::Service& service() override;

        static constexpr std::shared_ptr<const ServiceDefinition> definitionPtr()
        {
            if (!m_definition)
            {
                m_definition = std::make_shared<ServiceDefinition>();
                m_definition->name = name<TDerived>(true);
                m_definition->type = NodeDefinition::Type::Service;
                m_definition->serviceName = TGenerated::service_full_name();
            }

            return m_definition;
        }

        void setId(std::string id) override final
        {
            ServiceBase::setId(std::move(id));
        }

        // will use T::name() if it was implemented
        template <typename T>
        static auto name(bool)
            -> decltype(T::name, std::string{})
        {
            return T::name();
        }

        // will use service name as a default
        template <typename T>
        static auto name(int)
            -> std::string
        {
            #pragma message ("WARNING: A service has no static name(), will use the service name instead")
            return TGenerated::service_full_name();
        }

        static inline std::shared_ptr<ServiceDefinition> m_definition{nullptr};
    };

    template <typename TDerived, typename TGenerated>
    grpc::Service& Service<TDerived, TGenerated>::service()
    {
        return dynamic_cast<grpc::Service&>(*this);
    }
}
