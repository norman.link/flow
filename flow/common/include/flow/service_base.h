#pragma once

#include "node_base.h"

#include <grpc++/impl/codegen/service_type.h>

namespace flow
{
    struct ServiceDefinition : NodeDefinition
    {
        std::string serviceName;

        const google::protobuf::ServiceDescriptor& descriptor() const
        {
            static google::protobuf::ServiceDescriptor* descriptor = nullptr;
            if (!descriptor)
            {
                auto pool = google::protobuf::DescriptorPool::generated_pool();
                pool->FindServiceByName(serviceName);
            }

            if (!descriptor)
            {
                throw std::runtime_error("service descriptor not found");
            }

            return *descriptor;
        }
    };

    class ServiceBase
        : public NodeBase
    {
    public:
        ServiceBase(std::shared_ptr<const ServiceDefinition> definition);

        const ServiceDefinition& definition() const;
        virtual grpc::Service& service() = 0;

        void setId(std::string id) override;
    };
}
