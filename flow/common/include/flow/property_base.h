#pragma once

#include <string>
#include <google/protobuf/any.pb.h>
#include <google/protobuf/descriptor.pb.h>

namespace flow
{
    class PropertyBase
    {
    public:
        virtual ~PropertyBase() = default;

        const std::string& name() const
        {
            return m_name;
        }

        virtual const google::protobuf::FileDescriptor* descriptor() const = 0;
        virtual const google::protobuf::Any* toAny() const = 0;
        virtual void fromAny(const google::protobuf::Any& value) = 0;

    protected:
        PropertyBase(std::string name)
            : m_name{std::move(name)}
        {
        }

        PropertyBase& operator=(PropertyBase&& other)
        {
            if (this != &other)
            {
                m_name = std::move(other.m_name);
            }

            return *this;
        }

    private:
        std::string m_name;
    };
}
