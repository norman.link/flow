#pragma once

#include <flow/concurrency/coordinator.h>

#include <string>
#include <google/protobuf/message.h>
#include <rxcpp/rx.hpp>

namespace rx = rxcpp;

namespace flow
{
    class NodeBase;

    template <typename TMessage>
    using StreamData = std::shared_ptr<const TMessage>;

    class SlotBase
    {
    public:
        virtual ~SlotBase() = default;

        std::string name() const;
        const NodeBase& node() const;
        bool connectableTo(const SlotBase& other) const;

    protected:
        SlotBase(
            std::string name,
            const google::protobuf::Descriptor* descriptor,
            const NodeBase& node);

        const google::protobuf::Descriptor* descriptor() const;

        concurrency::Coordinator& coordinator() const;

    private:
        std::string m_name;
        const google::protobuf::Descriptor* m_descriptor;
        const NodeBase& m_node;
    };
}
