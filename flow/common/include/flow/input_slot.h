#pragma once

#include "slot_base.h"

namespace flow
{
    class OutputSlotBase;

    template <typename TMessage>
    class OutputSlot;

    template <typename TMessage>
    using InputStream = rx::observable<StreamData<TMessage>>;

    class InputSlotBase
        : public SlotBase
    {
    public:
        enum class State
        {
            Stopped,
            Waiting,
            Running
        };

        virtual bool operator==(const InputSlotBase& other) const;

        virtual void connectWith(OutputSlotBase& slot) = 0;
        virtual void disconnectFrom(OutputSlotBase& slot) = 0;

        virtual void start() = 0;
        virtual void stop() = 0;

        virtual rx::observable<bool> stopped() const = 0;
        virtual State state() const = 0;

    protected:
        InputSlotBase(
            std::string name,
            const google::protobuf::Descriptor* descriptor,
            const NodeBase& node);
    };

    template <typename TMessage>
    class InputSlot
        : public InputSlotBase
    {
    public:
        InputSlot(
            std::string name,
            const NodeBase& node)
            : InputSlotBase(std::move(name), TMessage::descriptor(), node)
            , m_state{State::Stopped}
            , m_requestedState{State::Waiting}
            , m_worker{coordinator().create_coordinator().get_worker()}
        {
            m_deferredFactory = [this]()
            {
                // new state is 'waiting'
                {
                    std::unique_lock<std::mutex> lock(m_stateMutex);
                    m_state = State::Waiting;
                    m_waitForStateChanged.notify_all();
                }

                // wait until either a connection should be made or the observable should be stopped
                {
                    std::unique_lock<std::mutex> lock(m_stateMutex);
                    m_waitForStateChanged.wait(lock,
                        [this]()
                        {
                            return m_requestedState == State::Running ||
                                m_requestedState == State::Stopped;
                        });

                    if (m_requestedState == State::Stopped)
                    {
                        return rx::observable<>::empty<StreamData<TMessage>>().as_dynamic();
                    }
                }

                // new state is 'running'
                {
                    std::unique_lock<std::mutex> lock(m_stateMutex);
                    m_state = State::Running;
                    m_waitForStateChanged.notify_all();
                }

                // merge all input observables
                rx::observable<StreamData<TMessage>> mergedObservable =
                    rx::observable<>::empty<StreamData<TMessage>>();

                {
                    std::unique_lock<std::mutex> lock(m_slotsMutex);
                    for (const auto& slot : m_connectedSlots)
                    {
                        mergedObservable = mergedObservable.merge(slot.get().observable());
                    }
                }

                auto disconnectObservable = m_disconnectSubject.get_observable()
                    .observe_on(rx::serialize_same_worker(m_worker));

                // take all input observables until it should be disconnected, then go
                // to 'waiting' again by deferring
                return mergedObservable
                    .take_until(disconnectObservable)
                    .concat(rx::observable<>::defer(m_deferredFactory))
                    .as_dynamic();
            };

            m_observable = rx::observable<>::defer(m_deferredFactory)
                .observe_on(rx::serialize_same_worker(m_worker))
                .subscribe_on(rx::serialize_same_worker(m_worker))
                .finally([this]()
                {
                    std::unique_lock<std::mutex> lock(m_stateMutex);
                    m_state = State::Stopped;
                    m_waitForStateChanged.notify_all();
                });
        }

        ~InputSlot()
        {
            stop();

            {
                std::unique_lock<std::mutex> lock(m_stateMutex);
                m_requestedState = State::Stopped;
                m_waitForStateChanged.notify_all();
                m_waitForStateChanged.wait(lock, [this](){ return m_state == State::Stopped; });
            }

            m_connectedSlots.clear();

            m_worker.unsubscribe();
        }

        rx::observable<StreamData<TMessage>>& observable()
        {
            return m_observable;
        }

        void start() override
        {
            {
                std::unique_lock<std::mutex> lock(m_slotsMutex);
                if (m_connectedSlots.empty())
                {
                    return;
                }
            }

            std::unique_lock<std::mutex> lock(m_stateMutex);
            if (m_requestedState == State::Running)
            {
                throw std::runtime_error("already running");
            }

            m_requestedState = State::Running;
            m_waitForStateChanged.notify_all();
        }

        void stop() override
        {
            std::unique_lock<std::mutex> lock(m_stateMutex);
            if (m_state != State::Running)
            {
                return;
            }

            m_disconnectSubject.get_subscriber().on_next(nullptr);
            m_requestedState = State::Waiting;
            m_waitForStateChanged.notify_all();
        }

        rx::observable<bool> stopped() const override
        {
            return rx::observable<>::create<bool>(
                [this](auto& subscriber)
                {
                    if (subscriber.is_subscribed())
                    {
                        // wait for change
                        {
                            std::unique_lock<std::mutex> lock(m_stateMutex);
                            m_waitForStateChanged.wait(lock,
                                [this]()
                                {
                                    return m_state == State::Waiting || m_state == State::Stopped;
                                });
                        }

                        subscriber.on_next(true);
                        subscriber.on_completed();
                    }
                });
        }

        State state() const override
        {
            std::unique_lock<std::mutex> lock(m_stateMutex);
            return m_state;
        }

        void connectWith(OutputSlotBase& slot) override
        {
            {
                std::unique_lock<std::mutex> lock(m_stateMutex);
                if (m_state == State::Running)
                {
                    throw std::runtime_error("cannot connect a busy input slot");
                }
            }

            std::unique_lock<std::mutex> lock(m_slotsMutex);
            m_connectedSlots.push_back(dynamic_cast<OutputSlot<TMessage>&>(slot));
        }

        void disconnectFrom(OutputSlotBase& slot) override
        {
            {
                std::unique_lock<std::mutex> lock(m_stateMutex);
                if (m_state == State::Running)
                {
                    throw std::runtime_error("cannot disconnect from a busy input slot");
                }
            }

            std::unique_lock<std::mutex> lock(m_slotsMutex);
            auto slotIt = std::find_if(m_connectedSlots.begin(), m_connectedSlots.end(),
                [&slot](const auto& curSlot)
                {
                    return slot == curSlot;
                });

            if (slotIt == m_connectedSlots.end())
            {
                throw std::runtime_error("output slot is not connected to input slot");
            }

            m_connectedSlots.erase(slotIt);
        }

    private:
        rx::rxsc::worker m_worker;
        rx::observable<StreamData<TMessage>> m_observable;
        rx::subjects::subject<StreamData<TMessage>> m_disconnectSubject;

        State m_state;
        State m_requestedState;
        mutable std::mutex m_stateMutex;
        mutable std::condition_variable m_waitForStateChanged;

        std::function<rx::observable<StreamData<TMessage>>()> m_deferredFactory;

        // list of connected slots
        std::mutex m_slotsMutex;
        std::vector<std::reference_wrapper<OutputSlot<TMessage>>> m_connectedSlots;
    };
}
