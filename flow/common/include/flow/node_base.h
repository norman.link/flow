#pragma once

#include "input_slot.h"
#include "output_slot.h"
#include "property.h"
#include "factory.h"
#include "plugin/plugin_interface.h"

#include <string>
#include <vector>
#include <functional>

namespace flow
{
    struct NodeDefinition
    {
        virtual ~NodeDefinition() = default;

        std::string name;

        enum class Type
        {
            Node,
            Service
        } type;
    };

    class NodeBase
    {
    public:
        NodeBase(std::shared_ptr<const NodeDefinition> definition);
        virtual ~NodeBase() = default;

        const NodeDefinition& definition() const;

        std::vector<std::reference_wrapper<InputSlotBase>> inputSlots() const;
        std::vector<std::reference_wrapper<OutputSlotBase>> outputSlots() const;
        std::vector<std::reference_wrapper<PropertyBase>> properties() const;

        virtual void setId(std::string id);
        std::string id() const;

    protected:
        template <typename TMessage>
        InputStream<TMessage> addInput(std::string name);

        template <typename TMessage>
        void addOutput(std::string name, OutputStream<TMessage> observable);

        template <typename TValue, typename... Args>
        Property<TValue> addProperty(std::string name, Args&&... args);

    private:
        std::string m_id;

        std::vector<std::unique_ptr<InputSlotBase>> m_inputSlots;
        std::vector<std::unique_ptr<OutputSlotBase>> m_outputSlots;
        std::vector<std::unique_ptr<PropertyBase>> m_properties;
        std::shared_ptr<const NodeDefinition> m_definition;
    };

    template <typename TMessage>
    InputStream<TMessage> NodeBase::addInput(std::string name)
    {
        auto slot = std::make_unique<InputSlot<TMessage>>(std::move(name), *this);
        auto& observable = slot->observable();
        m_inputSlots.push_back(std::move(slot));
        return observable;
    }

    template <typename TMessage>
    void NodeBase::addOutput(std::string name, OutputStream<TMessage> observable)
    {
        auto slot = std::make_unique<OutputSlot<TMessage>>(
            std::move(name), std::move(observable), *this);
        m_outputSlots.push_back(std::move(slot));
    }

    template <typename TValue, typename... Args>
    Property<TValue> NodeBase::addProperty(std::string name, Args&&... args)
    {
        auto property = std::make_unique<PropertyContainer<TValue>>(
            std::move(name), std::forward<Args>(args)...);
        Property<TValue> wrapper(*property);
        m_properties.push_back(std::move(property));
        return wrapper;
    }

    template <typename T>
    class NodeRegistrar
    {
    public:
        constexpr NodeRegistrar() noexcept
        {
            try
            {
                dynamic_cast<Factory&>(plugin::PluginInterface::get().factory()).registerNode<T>();
            }
            catch (const std::exception& e)
            {
                std::cerr << "could not register node: " << e.what() << std::endl;
            }
        }
    };
}
