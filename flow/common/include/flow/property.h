#pragma once

#include "property_container.h"

#include <rxcpp/rx-observable.hpp>
#include <optional>

namespace rx = rxcpp;

namespace flow
{
    template <typename TValue>
    class Property
    {
    public:
        class PropertyNotSetException
            : public std::runtime_error
        {
        public:
            PropertyNotSetException(const Property& property)
                : std::runtime_error("property is not initialized")
            {}
        };

        Property()
        {
        }

        Property(PropertyContainer<TValue>& property)
            : m_property{property}
        {
        }

        Property(Property<TValue>&& other)
        {
            *this = std::move(other);
        }

        Property<TValue>& operator=(Property<TValue>&& other)
        {
            if (this != &other)
            {
                m_property = std::move(other.m_property);
            }

            return *this;
        }

        const std::string& name() const
        {
            return property().name();
        }

        void query(std::function<void(const TValue&)> queryFunc) const
        {
            property().query(std::move(queryFunc));
        }

        void mutate(std::function<void(TValue&)> mutationFunc)
        {
            property().mutate(std::move(mutationFunc));
        }

        operator TValue() const
        {
            return value();
        }

        TValue value() const
        {
            return property().get();
        }

        void operator=(TValue&& value)
        {
            property().set(std::move(value));
        }

        rx::observable<TValue> observable() const
        {
            return property().observable();
        }

    private:
        PropertyContainer<TValue>& property() const
        {
            if (!m_property.has_value())
            {
                throw PropertyNotSetException(*this);
            }

            return m_property.value().get();
        }

        std::optional<std::reference_wrapper<PropertyContainer<TValue>>> m_property;
    };
}
