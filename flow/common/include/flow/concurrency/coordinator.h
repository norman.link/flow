#pragma once

#include <rxcpp/rx-includes.hpp>

namespace flow::concurrency
{
    class __declspec(dllexport) CoordinatorBase
    {
    public:
        virtual ~CoordinatorBase() = default;

    protected:
        CoordinatorBase() = default;
    };

    class  Coordinator
        : public rxcpp::coordination_base
        , public CoordinatorBase
    {
        class input_type
        {
            rxcpp::schedulers::worker controller;
            rxcpp::schedulers::scheduler factory;
            rxcpp::identity_one_worker coordination;

        public:
            explicit input_type(rxcpp::schedulers::worker w)
                : controller(w)
                , factory(rxcpp::schedulers::make_same_worker(w))
                , coordination(factory)
            {
            }

            inline rxcpp::schedulers::worker get_worker() const
            {
                return controller;
            }

            inline rxcpp::schedulers::scheduler get_scheduler() const
            {
                return factory;
            }

            inline rxcpp::schedulers::scheduler::clock_type::time_point now() const
            {
                return factory.now();
            }

            template<class Observable>
            auto in(Observable o) const
                -> decltype(o.observe_on(coordination))
            {
                return      o.observe_on(coordination);
            }

            template<class Subscriber>
            auto out(Subscriber s) const -> Subscriber
            {
                return s;
            }

            template<class F>
            auto act(F f) const -> F
            {
                return f;
            }
        };

        rxcpp::schedulers::scheduler m_factory;

    public:
        explicit Coordinator(rxcpp::schedulers::scheduler sc)
            : m_factory(sc)
        {
        }

        typedef rxcpp::coordinator<input_type> coordinator_type;

        inline rxcpp::schedulers::scheduler::clock_type::time_point now() const
        {
            return m_factory.now();
        }

        inline coordinator_type create_coordinator(rxcpp::composite_subscription cs = rxcpp::composite_subscription()) const
        {
            auto w = m_factory.create_worker(std::move(cs));
            return coordinator_type(input_type(std::move(w)));
        }
    };
}
