#pragma once

namespace flow
{
    template <typename T>
    class Singleton
    {
    public:
        static T& get()
        {
            volatile static T* instance = nullptr;

            if (!instance)
            {
                static T staticInstance;
                instance = &staticInstance;
            }

            return *((T*)instance);
        }
    };
}
