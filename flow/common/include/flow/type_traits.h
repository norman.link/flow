#pragma once

#include <type_traits>
#include <google/protobuf/message.h>
#include <google/protobuf/wrappers.pb.h>

namespace flow
{
    template <typename TValue, typename Enable = void>
    struct TypeTraits
    {
        static_assert("undefined type");
    };

    template <typename TValue>
    struct TypeTraits<TValue,
        typename std::enable_if<std::is_base_of<google::protobuf::Message, TValue>::value>::type>
    {
        using ProtobufType = TValue;

        static const TValue& get(const ProtobufType& proto)
        {
            return proto;
        }

        static TValue& get(ProtobufType& proto)
        {
            return proto;
        }

        static void set(TValue& value, ProtobufType& proto)
        {
            proto = value;
        }

        static void default(ProtobufType& proto)
        {
            // NOP: protobuf messages are initialized in their constructor
        }
    };

    template <>
    struct TypeTraits<bool>
    {
        using ProtobufType = google::protobuf::BoolValue;

        static bool get(const ProtobufType& proto)
        {
            return proto.value();
        }

        static void set(const bool& value, ProtobufType& proto)
        {
            proto.set_value(value);
        }

        static void default(ProtobufType& proto)
        {
            TypeTraits::set(false, proto);
        }
    };

    template <>
    struct TypeTraits<std::int32_t>
    {
        using ProtobufType = google::protobuf::Int32Value;

        static std::int32_t get(const ProtobufType& proto)
        {
            return static_cast<std::int32_t>(proto.value());
        }

        static void set(const std::int32_t& value, ProtobufType& proto)
        {
            proto.set_value(static_cast<google::protobuf::int32>(value));
        }

        static void default(ProtobufType& proto)
        {
            TypeTraits::set(0, proto);
        }
    };

    template <>
    struct TypeTraits<std::uint32_t>
    {
        using ProtobufType = google::protobuf::UInt32Value;

        static std::uint32_t get(const ProtobufType& proto)
        {
            return static_cast<std::uint32_t>(proto.value());
        }

        static void set(const std::uint32_t& value, ProtobufType& proto)
        {
            proto.set_value(static_cast<google::protobuf::uint32>(value));
        }

        static void default(ProtobufType& proto)
        {
            TypeTraits::set(0, proto);
        }
    };

    template <>
    struct TypeTraits<std::int64_t>
    {
        using ProtobufType = google::protobuf::Int64Value;

        static std::int64_t get(const ProtobufType& proto)
        {
            return static_cast<std::int64_t>(proto.value());
        }

        static void set(const std::int64_t& value, ProtobufType& proto)
        {
            proto.set_value(static_cast<google::protobuf::int64>(value));
        }

        static void default(ProtobufType& proto)
        {
            TypeTraits::set(0, proto);
        }
    };

    template <>
    struct TypeTraits<std::uint64_t>
    {
        using ProtobufType = google::protobuf::UInt64Value;

        static std::uint64_t get(const ProtobufType& proto)
        {
            return static_cast<std::uint64_t>(proto.value());
        }

        static void set(const std::uint64_t& value, ProtobufType& proto)
        {
            proto.set_value(static_cast<google::protobuf::uint64>(value));
        }

        static void default(ProtobufType& proto)
        {
            TypeTraits::set(0, proto);
        }
    };

    template <>
    struct TypeTraits<float>
    {
        using ProtobufType = google::protobuf::FloatValue;

        static float get(const ProtobufType& proto)
        {
            return proto.value();
        }

        static void set(const float& value, ProtobufType& proto)
        {
            proto.set_value(value);
        }

        static void default(ProtobufType& proto)
        {
            TypeTraits::set(0, proto);
        }
    };

    template <>
    struct TypeTraits<double>
    {
        using ProtobufType = google::protobuf::DoubleValue;

        static double get(const ProtobufType& proto)
        {
            return proto.value();
        }

        static void set(const double& value, ProtobufType& proto)
        {
            proto.set_value(value);
        }

        static void default(ProtobufType& proto)
        {
            TypeTraits::set(0, proto);
        }
    };

    template <>
    struct TypeTraits<std::string>
    {
        using ProtobufType = google::protobuf::StringValue;

        static std::string get(const ProtobufType& proto)
        {
            return proto.value();
        }

        static void set(const std::string& value, ProtobufType& proto)
        {
            proto.set_value(value);
        }

        static void default(ProtobufType& proto)
        {
            TypeTraits::set("", proto);
        }
    };
}
