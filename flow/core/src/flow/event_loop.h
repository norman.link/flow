#pragma once

#include <memory>
#include <queue>
#include <atomic>
#include <condition_variable>
#include <mutex>

namespace flow
{
    namespace events
    {
        class EventBase;
    }

    class EventLoop final
    {
    public:
        EventLoop();
        ~EventLoop();

        template <typename TEvent, typename... Args>
        void add(Args&&... args)
        {
            add(std::make_unique<TEvent>(std::forward<Args>(args)...));
        }

        void add(std::shared_ptr<events::EventBase>&& event);

        void run();
        void stop();

    private:
        std::atomic_bool m_terminate;
        std::queue<std::shared_ptr<events::EventBase>> m_events;
        std::condition_variable m_waitForEvent;
        std::mutex m_mutex;
    };
}
