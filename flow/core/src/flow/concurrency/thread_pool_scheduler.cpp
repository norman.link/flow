#include "thread_pool_scheduler.h"
#include "thread_pool_worker.h"
#include "thread_pool.h"

namespace flow::concurrency
{
    ThreadPoolScheduler::ThreadPoolScheduler(ThreadPool& threadPool)
        : m_threadPool{threadPool}
    {
    }

    rxcpp::schedulers::scheduler::clock_type::time_point ThreadPoolScheduler::now() const
    {
        return clock_type::now();
    }

    rxcpp::schedulers::worker ThreadPoolScheduler::create_worker(
        rxcpp::composite_subscription subscription) const
    {
        return rxcpp::schedulers::worker(subscription,
            std::make_shared<ThreadPoolWorker>(subscription, m_threadPool));
    }
}
