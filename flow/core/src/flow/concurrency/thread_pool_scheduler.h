#pragma once

#include <rxcpp/rx-includes.hpp>

namespace flow::concurrency
{
    class ThreadPool;

    class ThreadPoolScheduler
        : public rxcpp::schedulers::scheduler_interface
    {
    public:
        explicit ThreadPoolScheduler(ThreadPool& threadPool);

        clock_type::time_point now() const override;

        rxcpp::schedulers::worker create_worker(
            rxcpp::composite_subscription subscription) const override;

    private:
        ThreadPool& m_threadPool;
    };
}
