#pragma once

#include <vector>
#include <thread>
#include <condition_variable>
#include <queue>
#include <future>
#include <string>
#include <chrono>

namespace flow::concurrency
{
    class ThreadPool
    {
    public:
        ThreadPool(unsigned numThreads = std::thread::hardware_concurrency() - 1);
        virtual ~ThreadPool();

        using ClockType = std::chrono::steady_clock;
        using TimePoint = ClockType::time_point;

        template <typename TValue>
        std::future<TValue> execute(std::function<TValue()>&& task);

        template <typename TValue>
        std::future<TValue> schedule(TimePoint when, std::function<TValue()>&& task);

    private:
        std::atomic_bool m_terminate;
        std::mutex m_mutex;
        std::condition_variable m_waitForTaskAvailable;
        std::condition_variable m_waitForThreadAvailable;
        std::vector<std::thread> m_threads;

        using Task = std::function<void()>;
        using ScheduledTask = std::pair<TimePoint, Task>;
        
        struct CompareTime
        {
            bool operator()(const ScheduledTask& lhs, const ScheduledTask& rhs) const
            {
                return lhs.first > rhs.first;
            }
        };

        std::priority_queue<ScheduledTask, std::deque<ScheduledTask>, CompareTime> m_tasks;

        const unsigned m_tasksSize;
    };

    template <typename TValue>
    std::future<TValue> ThreadPool::execute(std::function<TValue()>&& task)
    {
        return schedule(ClockType::now(), std::move(task));
    }

    template <typename TValue>
    std::future<TValue> ThreadPool::schedule(ThreadPool::TimePoint when, std::function<TValue()>&& task)
    {
        // add a task to the queue and notify the next available thread
        std::unique_lock<std::mutex> lock(m_mutex);
        while (m_tasks.size() >= m_tasksSize)
        {
            m_waitForThreadAvailable.wait(lock);
        }

        auto packagedTask = std::make_shared<std::packaged_task<TValue()>>(std::move(task));
        auto future = packagedTask->get_future();

        auto runTask = [packagedTask{std::move(packagedTask)}]()
        {
            (*packagedTask)();
        };

        m_tasks.push(std::make_pair(std::move(when), std::move(runTask)));
        m_waitForTaskAvailable.notify_one();

        return future;
    }
}
