#include "thread_pool_worker.h"
#include "thread_pool.h"

namespace
{
    unsigned nextKey(const std::unordered_map<unsigned, std::future<void>>& map)
    {
        unsigned key = 0;

        for (auto it = map.cbegin(); it != map.cend(); it++)
        {
            if (it->first > key)
            {
                break;
            }
            else
            {
                key++;
            }
        }

        return key;
    }
}

namespace flow::concurrency
{
    ThreadPoolWorker::State::State(rxcpp::composite_subscription subscription, ThreadPool& threadPool)
        : lifetime(subscription)
        , threadPool{threadPool}
    {
    }

    ThreadPoolWorker::ThreadPoolWorker(
        rxcpp::composite_subscription subscription,
        ThreadPool& threadPool)
        : m_state{std::make_shared<State>(subscription, threadPool)}
    {
        auto keepAlive = m_state;

        m_state->lifetime.add(
            [keepAlive]()
            {
                // wait until all schedulables are finished
                std::unique_lock<std::mutex> lock(keepAlive->lock);
                while (!keepAlive->runningTasks.empty())
                {
                    auto& task = keepAlive->runningTasks.begin()->second;
                    lock.unlock();

                    if (task.valid())
                    {
                        task.get();
                    }

                    lock.lock();
                }
            });
    }

    rxcpp::schedulers::scheduler::clock_type::time_point ThreadPoolWorker::now() const
    {
        return clock_type::now();
    }

    void ThreadPoolWorker::schedule(const rxcpp::schedulers::schedulable& schedulable) const
    {
        schedule(now(), schedulable);
    }

    void ThreadPoolWorker::schedule(
        clock_type::time_point when,
        const rxcpp::schedulers::schedulable& schedulable) const
    {
        if (schedulable.is_subscribed())
        {
            std::unique_lock<std::mutex> lock(m_state->lock);

            auto keepAlive = m_state;
            unsigned taskId = nextKey(m_state->runningTasks);

            auto future = m_state->threadPool.schedule<void>(when,
                [this, schedulable, keepAlive, taskId]()
                {
                    if (schedulable.is_subscribed())
                    {
                        {
                            std::unique_lock<std::mutex> lock(keepAlive->lock);
                            m_state->recursion.reset(keepAlive->runningTasks.empty());
                        }

                        schedulable(m_state->recursion.get_recurse());
                    }

                    {
                        std::unique_lock<std::mutex> lock(keepAlive->lock);
                        keepAlive->runningTasks.erase(taskId);
                    }
                });

            m_state->runningTasks[taskId] = std::move(future);
            m_state->recursion.reset(false);
        }
    }
}
