#include "thread_pool.h"

#include <iostream>

#ifdef _MSC_VER
#include <Windows.h>
namespace
{
    const DWORD MS_VC_EXCEPTION = 0x406D1388;
#pragma pack(push, 8)
    typedef struct tagTHREADNAME_INFO
    {
        DWORD dwType;       // Must be 0x1000.
        LPCSTR szName;      // Pointer to name (in user addr space).
        DWORD dwThreadID;   // Thread ID (-1=caller thread).
        DWORD dwFlags;      // Reserved for future use, must be zero.
     } THREADNAME_INFO;
#pragma pack(pop)
}
#endif

namespace
{
    void setThreadName(const char* threadName)
    {
#ifdef _MSC_VER
        // https://docs.microsoft.com/de-de/visualstudio/debugger/how-to-set-a-thread-name-in-native-code

        THREADNAME_INFO info;
        info.dwType = 0x1000;
        info.szName = threadName;
        info.dwThreadID = -1;
        info.dwFlags = 0;

        __try
        {
            RaiseException(
                MS_VC_EXCEPTION,
                0,
                sizeof(info) / sizeof(ULONG_PTR),
                (ULONG_PTR*)&info);
        }
        __except (EXCEPTION_EXECUTE_HANDLER)
        {
        }
#endif
    }
}

namespace flow::concurrency
{
    ThreadPool::ThreadPool(unsigned numThreads)
        : m_terminate{false}
        , m_tasksSize{numThreads * 10}
    {
        for (unsigned i = 0; i < numThreads; i++)
        {
            // Add a thread to the thread pool. All threads will be waiting by default.
            m_threads.emplace_back(
                [this, i]()
                {
                    setThreadName(std::string("Thread Pool (Thread #" + std::to_string(i) + ")").c_str());

                    while (!m_terminate)
                    {
                        std::function<void()> task;

                        {
                            // wait until a task is available
                            std::unique_lock<std::mutex> lock(m_mutex);

                            // wait until a task is available
                            m_waitForThreadAvailable.notify_one();

                            // wait until the task is ready
                            while (!m_terminate && (m_tasks.empty() || m_tasks.top().first > ClockType::now()))
                            {
                                if (m_tasks.empty())
                                {
                                    m_waitForTaskAvailable.wait(lock);
                                }
                                else
                                {
                                    m_waitForTaskAvailable.wait_until(lock, m_tasks.top().first);
                                }
                            }

                            if (m_terminate)
                            {
                                continue;
                            }

                            // remove task from the queue
                            task = m_tasks.top().second;
                            m_tasks.pop();
                        }

                        // execute
                        try
                        {
                            task();
                        }
                        catch (const std::exception& e) // TODO: ThreadException
                        {
                            std::cerr << "thread could not be executed: " << e.what() << std::endl;
                        }
                    }
                });
        }
    }

    ThreadPool::~ThreadPool()
    {
        m_terminate = true;

        // signal all threads that they should complete
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            while (!m_tasks.empty())
            {
                m_tasks.pop();
            }
            m_waitForTaskAvailable.notify_all();
        }

        // wait for all threads
        for (auto& thread : m_threads)
        {
            thread.join();
        }
    }
}
