#pragma once

#include <rxcpp/rx-includes.hpp>

namespace flow::concurrency
{
    class ThreadPool;

    class ThreadPoolWorker
        : public rxcpp::schedulers::worker_interface
    {
    public:
        ThreadPoolWorker(rxcpp::composite_subscription subscription, ThreadPool& threadPool);

        clock_type::time_point now() const override;
        void schedule(const rxcpp::schedulers::schedulable& scbl) const override;

        void schedule(
            clock_type::time_point when,
            const rxcpp::schedulers::schedulable& schedulable) const override;

    private:
        struct State
        {
            explicit State(rxcpp::composite_subscription subscription, ThreadPool& threadPool);

            rxcpp::composite_subscription lifetime;
            std::mutex lock;
            std::unordered_map<unsigned, std::future<void>> runningTasks;
            rxcpp::schedulers::recursion recursion;
            ThreadPool& threadPool;
        };

        std::shared_ptr<State> m_state;
    };
}
