#include "plugin.h"

#include <flow/plugin/plugin_interface.h>
#include <flow/node_base.h>

#include <iostream>

namespace flow::plugin
{
    Plugin::Plugin(std::string filename,
        plugin::Version version,
        std::string buildTime,
        plugin::PluginInterface& pluginInterface)
        : m_filename{std::move(filename)}
        , m_version{std::move(version)}
        , m_buildTime{std::move(buildTime)}
        , m_pluginInterface(pluginInterface)
    {
        std::cout << "loaded plugin "
            << "\"" << m_filename << "\" ("
            << m_version.major << "."
            << m_version.minor << "."
            << m_version.patch << "), built "
            << m_buildTime << std::endl;
    }

    Plugin::~Plugin()
    {
        if (!m_nodes.empty())
        {
            auto numNodes = m_nodes.size();
            m_nodes.clear();
            std::cout << "unloaded plugin \"" << m_filename << "\""
                << " with " << numNodes << " nodes" << std::endl;
        }
    }

    const std::string& Plugin::filename() const
    {
        return m_filename;
    }

    const plugin::Version& Plugin::version() const
    {
        return m_version;
    }

    const std::string& Plugin::buildTime() const
    {
        return m_buildTime;
    }

    const plugin::PluginInterface& Plugin::pluginInterface() const
    {
        return m_pluginInterface;
    }

    void Plugin::addNode(std::unique_ptr<NodeBase> node)
    {
        m_nodes.push_back(std::move(node));
    }

    void Plugin::clear()
    {
        m_nodes.clear();
    }

    std::vector<std::string> Plugin::nodeIds() const
    {
        std::vector<std::string> ids;
        std::transform(m_nodes.begin(), m_nodes.end(), std::back_inserter(ids),
            [](const auto& node)
            {
                return node->id();
            }
        );
        return ids;
    }

    NodeBase& Plugin::node(std::string id) const
    {
        auto nodeIt = std::find_if(m_nodes.begin(), m_nodes.end(),
            [&id](const auto& node)
            {
                return node->id() == id;
            }
        );

        if (nodeIt == m_nodes.end())
        {
            throw std::runtime_error("node not found");
        }

        return *(nodeIt->get());
    }

#ifdef _WIN32
    PluginWin32::PluginWin32(std::string filename,
        plugin::Version version,
        std::string buildTime,
        plugin::PluginInterface& pluginInterface,
        HINSTANCE module)
        : Plugin(
            std::move(filename),
            std::move(version),
            std::move(buildTime),
            pluginInterface)
        , m_module(module)
    {
    }

    PluginWin32::~PluginWin32()
    {
        Plugin::~Plugin();
        FreeLibrary(m_module);
    }
#else
#pragma message("Plugin system is not yet implemented for different platforms")
#endif
}
