#pragma once

#include <flow/plugin/version.h>

#include <vector>
#include <memory>
#include <string>

#ifdef _WIN32
#include <Windows.h>
#endif

namespace flow
{
    class NodeBase;
}

namespace flow::plugin
{
    class PluginInterface;

    class Plugin
    {
    public:
        Plugin(std::string filename,
            plugin::Version version,
            std::string buildTime,
            plugin::PluginInterface& pluginInterface);

        virtual ~Plugin();

        const std::string& filename() const;
        const plugin::Version& version() const;
        const std::string& buildTime() const;
        const plugin::PluginInterface& pluginInterface() const;

        void addNode(std::unique_ptr<NodeBase> node);
        void clear();

        std::vector<std::string> nodeIds() const;
        NodeBase& node(std::string id) const;

    private:
        std::string m_filename;
        plugin::Version m_version;
        std::string m_buildTime;
        plugin::PluginInterface& m_pluginInterface;
        std::vector<std::unique_ptr<NodeBase>> m_nodes;
    };

#ifdef _WIN32
    class PluginWin32
        : public Plugin
    {
    public:
        PluginWin32(std::string filename,
            plugin::Version version,
            std::string buildTime,
            plugin::PluginInterface& pluginInterface,
            HINSTANCE module);

        ~PluginWin32();

    private:
        HINSTANCE m_module;
    };
#else
#pragma message("Plugin system is not yet implemented for different platforms")
#endif
}
