#pragma once

#include <vector>
#include <memory>

namespace flow::concurrency
{
    class Coordinator;
}

namespace flow::plugin
{
    class Plugin;

    class PluginRegistry
    {
    public:
        PluginRegistry(std::shared_ptr<concurrency::Coordinator> coordinator);
        ~PluginRegistry();

        std::vector<std::shared_ptr<Plugin>> plugins() const;
        void unload();

    private:
        struct Impl;
        std::unique_ptr<Impl> m_impl;
    };
}
