#include "plugin_registry.h"
#include "plugin.h"

#include <flow/concurrency/coordinator.h>
#include <flow/plugin/plugin_interface.h>
#include <flow/factory.h>
#include <flow/node_base.h>

#include <grpc++/impl/grpc_library.h>
#include <rxcpp/rx.hpp>

#ifdef _WIN32
#include <Windows.h>
#endif

namespace flow::plugin
{
#ifdef _WIN32
    struct PluginRegistry::Impl
    {
        Impl(std::shared_ptr<concurrency::Coordinator> coordinator)
        {
            WIN32_FIND_DATA fileData;
            HANDLE fileHandle = FindFirstFile(R"(*.dll)", &fileData);

            if (fileHandle == (void*)ERROR_INVALID_HANDLE ||
                fileHandle == (void*)ERROR_FILE_NOT_FOUND)
            {
                return;
            }

            do
            {
                HINSTANCE module = LoadLibrary(std::string(fileData.cFileName).c_str());

                if (!module)
                {
                    continue;
                }

                // flow dlls must have a version function
                typedef plugin::Version(__cdecl *VersionProc)(void);
                VersionProc versionFunc = (VersionProc)GetProcAddress(module, "version");

                typedef int(__cdecl *BuildProc)(char** const, int&);
                BuildProc buildFunc = (BuildProc)GetProcAddress(module, "build");

                // and every plugin has it's own factory
                typedef plugin::PluginInterface&(__cdecl *PluginInterfaceProc)(void);
                PluginInterfaceProc pluginInterfaceFunc = (PluginInterfaceProc)GetProcAddress(module, "interface");

                // if these functions are not available, it is probably not a plugin dll
                if (!versionFunc || !buildFunc || !pluginInterfaceFunc)
                {
                    FreeLibrary(module);
                    continue;
                }

                // get build time
                char* buf = nullptr;
                int len = 0;
                buildFunc(&buf, len);
                std::string buildTime(buf);
                delete[] buf;

                // get plugin interface
                plugin::PluginInterface& pluginInterface = pluginInterfaceFunc();

                // init plugin
                pluginInterface.init(coordinator);

                // store plugin information
                auto plugin = std::make_shared<PluginWin32>(
                    std::string(fileData.cFileName),
                    versionFunc(),
                    std::move(buildTime),
                    pluginInterface,
                    module);

                this->plugins.push_back(std::move(plugin));
            } while (FindNextFile(fileHandle, &fileData));

            FindClose(fileHandle);
        }

        std::vector<std::shared_ptr<Plugin>> plugins;
    };
#else
#pragma message("Plugin system is not yet implemented for different platforms")
#endif

    PluginRegistry::PluginRegistry(std::shared_ptr<concurrency::Coordinator> coordinator)
        : m_impl{std::make_unique<PluginRegistry::Impl>(std::move(coordinator))}
    {
    }

    PluginRegistry::~PluginRegistry()
    {
        unload();
    }

    std::vector<std::shared_ptr<Plugin>> PluginRegistry::plugins() const
    {
        return m_impl->plugins;
    }

    void PluginRegistry::unload()
    {
        m_impl->plugins.clear();
    }
}
