#include "rpc_handler.h"
#include "static_server.h"

#include <mutex>
#include <iostream>

namespace flow::rpc
{
    void RpcHandler::clear()
    {
        m_servers.clear();
    }

    StaticServer& RpcHandler::server(std::string name) const
    {
        auto serverIt = m_servers.find(name);
        if (serverIt == m_servers.end())
        {
            throw std::runtime_error("could not find server \"" + name + "\"");
        }

        return *serverIt->second;
    }

    void RpcHandler::start()
    {
        for (auto& server : m_servers)
        {
            server.second->start();
        }
    }

    void RpcHandler::stop()
    {
        for (auto& server : m_servers)
        {
            server.second->stop();
        }
    }
}
