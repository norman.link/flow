#pragma once

#include <vector>
#include <thread>

namespace flow::rpc
{
    class StaticServer;

    class RpcHandler
    {
    public:
        RpcHandler() = default;
        ~RpcHandler() = default;

        void clear();

        template <typename... Args>
        void addServer(std::string name, Args&&... args)
        {
            auto server = std::make_unique<StaticServer>(std::forward<Args>(args)...);
            m_servers[name] = std::move(server);
        }

        StaticServer& server(std::string name) const;

        void start();
        void stop();

    private:
        std::unordered_map<std::string, std::unique_ptr<StaticServer>> m_servers;
    };
}
