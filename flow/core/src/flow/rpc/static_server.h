#pragma once

#include "server_base.h"

#include <string>
#include <memory>
#include <variant>
#include <grpc++/impl/codegen/service_type.h>
#include <grpc++/impl/rpc_service_method.h>
#include <grpc++/impl/channel_argument_option.h>

namespace flow::rpc
{
    class StaticServer
        : public ServerBase
    {
    public:
        StaticServer(unsigned port);
        ~StaticServer();

        std::shared_ptr<grpc::Channel> inProcessChannel(const grpc::ChannelArguments& args) const;

        void registerService(std::string name, grpc::Service& service)
        {
            m_services.push_back({std::move(name), service});
        }

        void clear()
        {
            m_services.clear();
        }

        template <typename TService, typename... Args>
        void registerService(Args&&... args)
        {
            if (m_server)
            {
                throw std::runtime_error("can only add services without running server");
            }

            const std::string& name = TService::name();
            const auto serviceIt = std::find_if(m_services.begin(), m_services.end(),
                [&name](const auto& service)
                {
                    return service.name == name;
                });

            if (serviceIt != m_services.end())
            {
                throw std::runtime_error("service \"" + name + "\" is already registered");
            }

            m_services.push_back({name, std::make_shared<TService>(std::forward<Args>(args)...)});
        }

    private:
        void initialize() override;
        void run() override;
        void close() override;

        std::unique_ptr<grpc::Server> m_server;

        struct Service
        {
            using ServicePtr = std::shared_ptr<grpc::Service>;
            using ServiceRef = std::reference_wrapper<grpc::Service>;

            std::string name;

            std::variant<
                ServicePtr,
                ServiceRef> service;

            grpc::Service* getService() const
            {
                if (std::holds_alternative<ServicePtr>(service))
                {
                    return std::get<ServicePtr>(service).get();
                }
                else if (std::holds_alternative<ServiceRef>(service))
                {
                    return &std::get<ServiceRef>(service).get();
                }

                return nullptr;
            }
        };

        std::vector<Service> m_services;
    };
}
