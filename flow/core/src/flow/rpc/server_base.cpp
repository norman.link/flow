#include "server_base.h"

#include <iostream>

namespace flow::rpc
{
    ServerBase::ServerBase(unsigned port)
        : m_port{port}
        , m_state{StoppedState{}}
    {
    }

    ServerBase::~ServerBase()
    {
    }

    unsigned ServerBase::port() const
    {
        return m_port;
    }

    void ServerBase::start()
    {
        try
        {
            std::get<StoppedState>(m_state);

            auto newState = RunningState{};

            initialize();

            newState.thread = std::thread(
                [this]()
                {
                    run();
                });

            m_state = std::move(newState);
        }
        catch (std::bad_variant_access&)
        {
            std::cerr << "server already started" << std::endl;
        }
        catch (std::runtime_error& e)
        {
            std::cerr << "could not start server on port " << port()
                << ": " << e.what() << std::endl;
        }
    }

    void ServerBase::stop()
    {
        try
        {
            auto& state = std::get<RunningState>(m_state);
            close();

            if (state.thread.joinable())
            {
                state.thread.join();
            }

            m_state = StoppedState{};
        }
        catch (std::bad_variant_access&)
        {
            std::cout << "server already stopped" << std::endl;
        }
    }

    bool ServerBase::running() const
    {
        return std::holds_alternative<RunningState>(m_state);
    }
}
