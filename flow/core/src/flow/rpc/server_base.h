#pragma once

#include <string>
#include <thread>
#include <variant>

namespace flow::rpc
{
    class ServerBase
    {
    public:
        virtual ~ServerBase();

        unsigned port() const;

        void start();
        void stop();
        bool running() const;

    protected:
        ServerBase(unsigned port);

        virtual void initialize() = 0;
        virtual void run() = 0;
        virtual void close() = 0;

    private:
        const unsigned m_port;

        struct RunningState
        {
            std::thread thread;
        };

        struct StoppedState
        {
        };

        std::variant<
            RunningState,
            StoppedState> m_state;
    };
}
