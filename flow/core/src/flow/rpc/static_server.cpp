#include "static_server.h"

#include <grpc++/server_builder.h>
#include <grpc++/security/server_credentials.h>
#include <grpc++/server.h>

namespace flow::rpc
{
    StaticServer::StaticServer(unsigned port)
        : ServerBase(std::move(port))
        , m_server{nullptr}
    {
    }

    StaticServer::~StaticServer()
    {
        if (running())
        {
            stop();
        }
    }

    void StaticServer::initialize()
    {
        if (m_server)
        {
            throw std::runtime_error("server is already running");
        }

        if (m_services.empty())
        {
            throw std::runtime_error("server has no registered services");
        }

        const std::string host = "localhost:" + std::to_string(port());

        grpc::ServerBuilder builder;
        builder.AddListeningPort(host, grpc::InsecureServerCredentials());

        for (const auto& service : m_services)
        {
            builder.RegisterService(service.getService());
        }

        m_server = builder.BuildAndStart();
    }

    void StaticServer::run()
    {
        if (!m_server)
        {
            throw std::runtime_error("server instance is invalid");
        }

        try
        {
            m_server->Wait();
        }
        catch (const std::exception& e)
        {
            std::cerr << "grpc server exception: " << e.what() << std::endl;
        }
    }

    void StaticServer::close()
    {
        if (m_server)
        {
            m_server->Shutdown();
            m_server = nullptr;
        }
    }

    std::shared_ptr<grpc::Channel> StaticServer::inProcessChannel(const grpc::ChannelArguments& args) const
    {
        if (!m_server)
        {
            return nullptr;
        }

        return m_server->InProcessChannel(args);
    }
}
