#include "event_loop.h"

#include <flow/events/event_base.h>

#include <iostream>

namespace flow
{
    EventLoop::EventLoop()
        : m_terminate{false}
    {
    }

    EventLoop::~EventLoop()
    {
        stop();
    }
    
    void EventLoop::add(std::shared_ptr<events::EventBase>&& event)
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_events.push(event);
        m_waitForEvent.notify_all();
    }

    void EventLoop::run()
    {
        std::cout << "event loop started" << std::endl;

        while (!m_terminate)
        {
            std::shared_ptr<events::EventBase> event;

            {
                // wait until events are available
                std::unique_lock<std::mutex> lock(m_mutex);
                while (m_events.empty())
                {
                    m_waitForEvent.wait(lock);
                }

                // get next event
                if (!m_events.empty())
                {
                    event = std::move(m_events.front());
                    m_events.pop();
                }
            }

            // execute event
            if (event)
            {
                try
                {
                    (*event)();
                }
                catch (const std::exception& e) // TODO: EventException
                {
                    std::cerr << "event could not be executed: " << e.what() << std::endl;
                }
            }
        }

        std::cout << "event loop stopped" << std::endl;
    }

    void EventLoop::stop()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_terminate = true;
        while (!m_events.empty())
        {
            m_events.pop();
        }
    }
}
