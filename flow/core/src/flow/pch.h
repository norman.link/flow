// shared precompiled headers
#include <flow/core/pch.h>

// framework includes
#include <flow/concurrency/coordinator.h>
#include <flow/concurrency/thread_pool.h>
#include <flow/events/event_base.h>
#include <flow/exceptions/service_exception.h>
#include <flow/rpc/rpc_handler.h>
#include <flow/rpc/server_base.h>
#include <flow/rpc/static_server.h>
#include <flow/plugin/plugin.h>
#include <flow/plugin/plugin_registry.h>
#include <flow/plugin/plugin_interface.h>
#include <flow/plugin/version.h>
#include <flow/event_loop.h>
#include <flow/input_slot.h>
#include <flow/node.h>
#include <flow/node_base.h>
#include <flow/output_slot.h>
#include <flow/service.h>
#include <flow/service_base.h>
#include <flow/slot_base.h>

// grpc common includes
#include <grpc++/impl/grpc_library.h>
#include <grpc++/server_builder.h>
#include <grpc++/security/server_credentials.h>
#include <grpc++/server.h>
#include <grpc++/impl/channel_argument_option.h>

// rxcpp includes
#include <rxcpp/rx.hpp>

// stl includes
#include <memory>
#include <functional>
#include <string>
#include <vector>
#include <thread>
#include <mutex>
#include <optional>
