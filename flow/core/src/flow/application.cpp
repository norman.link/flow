#include "event_loop.h"

#include <flow/plugin/plugin.h>
#include <flow/plugin/plugin_registry.h>
#include <flow/concurrency/coordinator.h>
#include <flow/concurrency/thread_pool.h>
#include <flow/concurrency/thread_pool_scheduler.h>
#include <flow/events/event_shutdown.h>
#include <flow/logging/logger.h>
#include <flow/rpc/static_server.h>
#include <flow/rpc/rpc_handler.h>
#include <flow/services/application.h>
#include <flow/services/graph.h>
#include <flow/application.h>
#include <flow/factory.h>
#include <flow/node_base.h>

#include <algorithm>
#include <iostream>

#if (defined _MSC_VER && defined _DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

namespace flow
{
    struct Application::Impl
    {
        Impl(int port)
            : threadPool{std::make_unique<concurrency::ThreadPool>()}
            , eventLoop{std::make_unique<EventLoop>()}
            , internalChannel{nullptr}
            , rpcHandler{std::make_unique<rpc::RpcHandler>()}
        {
            logger = std::make_unique<logging::Logger>();

            scheduler = std::make_shared<concurrency::ThreadPoolScheduler>(*threadPool);
            coordinator = std::make_shared<concurrency::Coordinator>(rxcpp::schedulers::scheduler(
                std::dynamic_pointer_cast<rxcpp::schedulers::scheduler_interface>(scheduler)));
            pluginRegistry = std::make_unique<plugin::PluginRegistry>(coordinator);

            internalServer = std::make_unique<rpc::StaticServer>(port);
            internalServer->registerService<services::Application>(*eventLoop, *rpcHandler);
            internalServer->registerService<services::Graph>(*pluginRegistry, *rpcHandler);
            internalServer->start();

            grpc::ChannelArguments args;
            internalChannel = internalServer->inProcessChannel(args);
        }

        void run()
        {
            if (!eventLoop)
            {
                throw std::runtime_error("event loop is invalid");
            }

            eventLoop->run();
        }

        std::shared_ptr<grpc::Channel> channel() const
        {
            return internalChannel;
        }

        void shutdown()
        {
            if (eventLoop)
            {
                eventLoop->add<events::EventShutdown>(*eventLoop);
            }
        }

        void cleanup()
        {
            // rpc handler needs to go first
            internalChannel = nullptr;
            internalServer = nullptr;
            rpcHandler = nullptr;

            // unload plugins
            pluginRegistry->unload();

            // explicitly shut down event loop and thread pool before unloading plugins
            // in order to completely free shared messages beforehand
            eventLoop = nullptr;
            threadPool = nullptr;

            pluginRegistry = nullptr;

            google::protobuf::ShutdownProtobufLibrary();

            logger = nullptr;
        }

    private:
        std::unique_ptr<logging::Logger> logger;
        std::unique_ptr<plugin::PluginRegistry> pluginRegistry;
        std::unique_ptr<EventLoop> eventLoop;
        std::unique_ptr<concurrency::ThreadPool> threadPool;
        std::shared_ptr<concurrency::ThreadPoolScheduler> scheduler;
        std::shared_ptr<concurrency::Coordinator> coordinator;
        std::unique_ptr<rpc::StaticServer> internalServer;
        std::unique_ptr<rpc::RpcHandler> rpcHandler;
        std::shared_ptr<grpc::Channel> internalChannel;
    };

    Application::Application(int port)
    {
#if (defined _MSC_VER && defined _DEBUG)
        _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
        printInfo();
        m_impl = std::make_unique<Application::Impl>(port);
    }

    Application::~Application()
    {
        m_impl->shutdown();
    }

    void Application::printInfo()
    {
        std::cout << "flow 0.0.1 (";
#ifdef _DEBUG
        std::cout << "Debug";
#else
        std::cout << "Release";
#endif
        std::cout << "), built " << __DATE__ << " " << __TIME__ << std::endl;
    }

    int Application::run()
    {
        try
        {
            // run main event loop
            m_impl->run();

            // cleanup plugins
            m_impl->cleanup();
        }
        catch (const std::exception& e)
        {
            std::cerr << e.what() << std::endl;

            try
            {
                m_impl->shutdown();
                m_impl->cleanup();
            }
            catch (const std::exception& e)
            {
                std::cerr << "could not clean up: " << e.what() << std::endl;
            }

            return 1;
        }

        return 0;
    }
    
    void Application::shutdown()
    {
        m_impl->shutdown();
    }

    std::shared_ptr<grpc::Channel> Application::inProcessChannel() const
    {
        return m_impl->channel();
    }
}
