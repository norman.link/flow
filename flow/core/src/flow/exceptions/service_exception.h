#pragma once

#include <stdexcept>
#include <grpc++/impl/codegen/status.h>

namespace flow::exceptions
{
    class ServiceException
        : public std::runtime_error
    {
    public:
        ServiceException(grpc::StatusCode statusCode, std::string message)
            : std::runtime_error(message.c_str())
            , m_statusCode{std::move(statusCode)}
        {
        }

        const grpc::StatusCode& statusCode() const
        {
            return m_statusCode;
        }

        grpc::Status status() const
        {
            return grpc::Status(m_statusCode, what());
        }

    private:
        grpc::StatusCode m_statusCode;
    };
}
