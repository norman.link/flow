#include "logger.h"

#include <iostream>

namespace flow::logging
{
    Logger::Logger()
        : m_default{std::cout}
        , m_error{std::cerr}
        , m_log{std::clog}
    {
    }

    Logger::~Logger()
    {
    }
}
