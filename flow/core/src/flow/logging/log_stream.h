#pragma once

#include <ostream>
#include <unordered_map>
#include <thread>
#include <mutex>

namespace flow::logging
{
    class LogStream
        : public std::ostream
        , public std::streambuf
    {
    public:
        LogStream(std::ostream& stream);
        ~LogStream();

        std::streambuf::int_type overflow(std::streambuf::int_type c) override;

    private:
        std::ostream& m_stream;
        std::streambuf* m_streamBuf;

        std::mutex m_mutex;
        std::unordered_map<std::thread::id, std::string> m_threadBuffer;
    };
}
