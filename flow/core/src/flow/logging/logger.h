#pragma once

#include "log_stream.h"

namespace flow::logging
{
    class Logger
    {
    public:
        Logger();
        ~Logger();

    private:
        LogStream m_default;
        LogStream m_error;
        LogStream m_log;
    };
}
