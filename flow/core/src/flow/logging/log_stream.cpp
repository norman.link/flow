#include "log_stream.h"

namespace flow::logging
{
    LogStream::LogStream(std::ostream& stream)
        : std::ostream(this)
        , m_stream{stream}
    {
        m_streamBuf = m_stream.rdbuf(this);
    }

    LogStream::~LogStream()
    {
        m_stream.rdbuf(m_streamBuf);
    }

    std::streambuf::int_type LogStream::overflow(std::streambuf::int_type c)
    {
        const auto& threadId = std::this_thread::get_id();

        {
            std::unique_lock<std::mutex> lock(m_mutex);
            std::string& buffer = m_threadBuffer[threadId];
            buffer += c;

            if (c == '\n')
            {
                m_streamBuf->sputn(buffer.data(), buffer.size());
                m_threadBuffer.erase(threadId);
            }
        }

        return c;
    }
}
