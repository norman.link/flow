#include "event_shutdown.h"

#include <flow/event_loop.h>

namespace flow::events
{
    EventShutdown::EventShutdown(EventLoop& eventLoop)
        : m_eventLoop{eventLoop}
    {
    }

    void EventShutdown::operator()()
    {
        m_eventLoop.stop();
    }
}
