#pragma once

#include "event_base.h"

namespace flow
{
    class EventLoop;
}

namespace flow::events
{
    class EventShutdown
        : public EventBase
    {
    public:
        EventShutdown(EventLoop& eventLoop);

        void operator()() override;

    private:
        EventLoop& m_eventLoop;
    };
}
