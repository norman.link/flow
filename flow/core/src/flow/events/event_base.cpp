#include "event_base.h"

namespace flow::events
{
    Event::Event(std::function<void()> func)
        : m_func{func}
    {
    }

    void Event::operator()()
    {
        m_func();
    }
}
