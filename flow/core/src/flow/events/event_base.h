#pragma once

#include <memory>
#include <functional>

namespace flow::events
{
    class EventBase
    {
    public:
        virtual ~EventBase() = default;

        virtual void operator()() = 0;

    protected:
        EventBase() = default;
    };

    class Event
        : public EventBase
    {
    public:
        Event(std::function<void()> func);

        void operator()() override;

    private:
        std::function<void()> m_func;
    };
}
