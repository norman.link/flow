#include "application.h"

#include <flow/event_loop.h>
#include <flow/events/event_shutdown.h>
#include <flow/rpc/rpc_handler.h>
#include <flow/rpc/static_server.h>

namespace flow::services
{
    Application::Application(EventLoop& eventLoop, rpc::RpcHandler& rpcHandler)
        : m_eventLoop{eventLoop}
        , m_rpcHandler{rpcHandler}
    {
    }

    grpc::Status Application::Configure(
        grpc::ServerContext* context,
        const generated::Configuration* request,
        google::protobuf::Empty* response)
    {
        // TODO: must not be done while graph is running
        m_rpcHandler.clear();

        for (const auto& channel : request->channels())
        {
            m_rpcHandler.addServer(channel.name(), channel.port());
        }

        return grpc::Status(grpc::StatusCode::OK, "");
    }

    grpc::Status Application::GetInformation(
        grpc::ServerContext* context,
        const google::protobuf::Empty* request,
        generated::Information* response)
    {
        // TODO
        return grpc::Status(grpc::StatusCode::OK, "");
    }

    grpc::Status Application::Shutdown(
        grpc::ServerContext* context,
        const google::protobuf::Empty* request,
        google::protobuf::Empty* response)
    {
        m_eventLoop.add<events::EventShutdown>(m_eventLoop);
        return grpc::Status(grpc::StatusCode::OK, "");
    }
}
