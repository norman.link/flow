#include "graph.h"

#include <flow/exceptions/service_exception.h>
#include <flow/plugin/plugin_registry.h>
#include <flow/plugin/plugin.h>
#include <flow/plugin/plugin_interface.h>
#include <flow/rpc/rpc_handler.h>
#include <flow/rpc/static_server.h>
#include <flow/node_base.h>
#include <flow/service_base.h>

#include <google/protobuf/descriptor.h>

namespace
{
    template <typename TSlot>
    TSlot& findSlot(
        std::string slotId,
        const std::vector<std::reference_wrapper<TSlot>>& slots)
    {
        auto slotIt = std::find_if(slots.begin(), slots.end(),
            [&slotId](const auto& slot)
            {
                return slot.get().name() == slotId;
            }
        );

        if (slotIt == slots.end())
        {
            throw std::runtime_error("slot \"" + slotId + "\" not found");
        }

        return slotIt->get();
    }
}

namespace flow::services
{
    Graph::Graph(plugin::PluginRegistry& pluginRegistry, rpc::RpcHandler& rpcHandler)
        : m_pluginRegistry{pluginRegistry}
        , m_rpcHandler{rpcHandler}
        , m_state{State::Stopped}
    {
        // build a list of available nodes for each plugin
        for (const auto& plugin : m_pluginRegistry.plugins())
        {
            auto nodes = plugin->pluginInterface().factory().list();
            for (const auto& node : nodes)
            {
                const auto& it = m_availableNodes.find(node);
                if (it != m_availableNodes.end())
                {
                    std::shared_ptr<plugin::Plugin> nodePlugin = it->second.lock();
                    if (!nodePlugin)
                    {
                        m_availableNodes.erase(it);
                    }
                    else
                    {
                        std::cerr << "node \"" << node << "\" already exists in plugin \""
                            << nodePlugin->filename() << "\"" << std::endl;
                    }
                }
                else
                {
                    m_availableNodes[node] = plugin;
                }
            }
        }
    }

    Graph::~Graph()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        if (m_state == State::Running)
        {
            stop();
        }
    }

    grpc::Status Graph::Start(
        grpc::ServerContext* context,
        const google::protobuf::Empty* request,
        google::protobuf::Empty* response)
    {
        return call([this]()
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            if (m_state == State::Running)
            {
                throw exceptions::ServiceException(
                    grpc::StatusCode::FAILED_PRECONDITION,
                    "cannot start graph, already running");
            }

            try
            {
                m_rpcHandler.start();

                for (const auto& plugin : m_pluginRegistry.plugins())
                {
                    auto nodeIds = plugin->nodeIds();
                    for (const auto& nodeId : nodeIds)
                    {
                        const auto& node = plugin->node(nodeId);
                        for (auto& slot : node.inputSlots())
                        {
                            slot.get().start();
                        }
                    }
                }

                m_state = State::Running;
            }
            catch (const std::exception& e)
            {
                m_state = State::Stopped;

                throw exceptions::ServiceException(
                    grpc::StatusCode::INTERNAL,
                    "could not start graph: " + std::string(e.what()));
            }
        });
    }

    grpc::Status Graph::Stop(
        grpc::ServerContext* context,
        const google::protobuf::Empty* request,
        google::protobuf::Empty* response)
    {
        return call([this]()
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            stop();
        });
    }

    void Graph::stop()
    {
        if (m_state == State::Running)
        {
            try
            {
                m_rpcHandler.stop();

                rx::observable<bool> allNodesStopped = rx::observable<>::just(true);
                for (const auto& plugin : m_pluginRegistry.plugins())
                {
                    auto nodeIds = plugin->nodeIds();
                    for (const auto& nodeId : nodeIds)
                    {
                        const auto& node = plugin->node(nodeId);
                        for (auto& slot : node.inputSlots())
                        {
                            allNodesStopped = allNodesStopped.merge(slot.get().stopped());

                            slot.get().stop();
                        }
                    }
                }

                // wait until all nodes are stopped
                allNodesStopped
                    .as_blocking()
                    .subscribe();
            }
            catch (const std::exception& e)
            {
                throw exceptions::ServiceException(
                    grpc::StatusCode::INTERNAL,
                    "could not stop graph: " + std::string(e.what()));
            }
        }

        m_state = State::Stopped;
    }

    grpc::Status Graph::Reset(
        grpc::ServerContext* context,
        const google::protobuf::Empty* request,
        google::protobuf::Empty* response)
    {
        return call([this]()
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            if (m_state == State::Running)
            {
                throw exceptions::ServiceException(
                    grpc::StatusCode::FAILED_PRECONDITION,
                    "cannot reset running graph");
            }

            try
            {
                for (const auto& plugin : m_pluginRegistry.plugins())
                {
                    plugin->clear();
                }
            }
            catch (const std::exception& e)
            {
                throw exceptions::ServiceException(
                    grpc::StatusCode::INTERNAL,
                    "could not reset graph: " + std::string(e.what()));
            }
        });
    }

    grpc::Status Graph::CreateNode(
        grpc::ServerContext* context,
        const generated::Node* request,
        google::protobuf::Empty* response)
    {
        return call([this, &request]()
        {
            std::string name = request->name();
            std::string id = request->id();

            std::unique_lock<std::mutex> lock(m_mutex);
            if (m_state == State::Running)
            {
                throw exceptions::ServiceException(
                    grpc::StatusCode::FAILED_PRECONDITION,
                    "cannot create a node on a running graph");
            }

            if (m_createdNodes.count(id))
            {
                throw exceptions::ServiceException(
                    grpc::StatusCode::INVALID_ARGUMENT,
                    "node with id \"" + id + "\" already exists");
            }

            const auto& nodeIt = m_availableNodes.find(name);
            if (nodeIt == m_availableNodes.end())
            {
                throw exceptions::ServiceException(
                    grpc::StatusCode::NOT_FOUND,
                    "node with name \"" + name + "\" not found");
            }

            const auto& plugin = nodeIt->second.lock();
            if (!plugin)
            {
                throw exceptions::ServiceException(
                    grpc::StatusCode::INTERNAL,
                    "plugin invalid");
            }

            // create node instance
            auto node = plugin->pluginInterface().factory().make(std::move(name));
            node->setId(id);
            plugin->addNode(std::move(node));
            m_createdNodes[std::move(id)] = plugin;
        });
    }

    grpc::Status Graph::SetServiceChannel(
        grpc::ServerContext* context,
        const generated::ServiceChannel* request,
        google::protobuf::Empty* response)
    {
        return call([this, &request]()
        {
            auto& server = m_rpcHandler.server(request->channel().name());
            auto& node = this->node(request->service().id());

            try
            {
                auto& service = dynamic_cast<ServiceBase&>(node);
                server.registerService(service.id(), service.service());
            }
            catch (...)
            {
                throw exceptions::ServiceException(
                    grpc::StatusCode::INTERNAL,
                    "node \"" + node.id() + "\" is not a service");
            }
        });
    }

    grpc::Status Graph::GetNodeProperties(
        grpc::ServerContext* context,
        const generated::Node* request,
        generated::NodeProperties* response)
    {
        return call([this, &request, &response]()
        {
            const auto& node = this->node(request->id());

            for (auto& property : node.properties())
            {
                auto newNodeProperty = response->add_properties();

                newNodeProperty->mutable_node()->CopyFrom(*request);

                auto newProperty = newNodeProperty->mutable_property();
                newProperty->set_name(property.get().name());
                property.get().descriptor()->CopyTo(newProperty->mutable_proto());

                auto value = property.get().toAny();
                newProperty->mutable_value()->CopyFrom(*value);
                delete value;
            }
        });
    }

    grpc::Status Graph::SetNodeProperty(
        grpc::ServerContext* context,
        const generated::NodeProperty* request,
        google::protobuf::Empty* response)
    {
        return call([this, &request]()
        {
            const auto& node = this->node(request->node().id());
            const auto& propertyName = request->property().name();

            const auto& properties = node.properties();
            const auto& propertyIt = std::find_if(properties.begin(), properties.end(),
                [&propertyName](const auto& property)
                {
                    return property.get().name() == propertyName;
                });

            if (propertyIt == properties.end())
            {
                throw exceptions::ServiceException(grpc::StatusCode::NOT_FOUND,
                    "property \"" + propertyName + "\" not found in node \"" + node.id() + "\"");
            }

            try
            {
                propertyIt->get().fromAny(request->property().value());
            }
            catch (const std::runtime_error&)
            {
                throw exceptions::ServiceException(grpc::StatusCode::INTERNAL,
                    "could not set property \"" + propertyName + "\" in node \"" + node.id() + "\"");
            }
        });
    }

    grpc::Status Graph::ConnectNodes(
        grpc::ServerContext* context,
        const generated::NodeConnection* request,
        google::protobuf::Empty* response)
    {
        return call([this, &request]()
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            if (m_state == State::Running)
            {
                throw exceptions::ServiceException(
                    grpc::StatusCode::FAILED_PRECONDITION,
                    "cannot connected nodes on a running graph");
            }

            const auto& from = request->from().node().id();
            const auto& output = request->from().slot().name();
            const auto& to = request->to().node().id();
            const auto& input = request->to().slot().name();

            auto& fromNode = node(std::move(from));
            auto& outputSlot = findSlot(std::move(output), fromNode.outputSlots());
            auto& toNode = node(std::move(to));
            auto& inputSlot = findSlot(std::move(input), toNode.inputSlots());

            inputSlot.connectWith(outputSlot);
        });
    }

    NodeBase& Graph::node(NodeId id) const
    {
        const auto& nodeIt = m_createdNodes.find(id);
        if (nodeIt == m_createdNodes.end())
        {
            throw exceptions::ServiceException(
                grpc::StatusCode::NOT_FOUND,
                "node with id \"" + id + "\" not found");
        }

        const auto& plugin = nodeIt->second.lock();
        if (!plugin)
        {
            throw exceptions::ServiceException(
                grpc::StatusCode::INTERNAL,
                "plugin invalid");
        }

        return plugin->node(std::move(id));
    }
}
