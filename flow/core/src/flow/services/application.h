#pragma once

#include <application.grpc.pb.h>

namespace flow
{
    class EventLoop;
}

namespace flow::rpc
{
    class RpcHandler;
}

namespace flow::services
{
    class Application
        : public generated::Application::Service
    {
    public:
        Application(EventLoop& eventLoop, rpc::RpcHandler& rpcHandler);

        static constexpr char const* name()
        {
            return generated::Application::service_full_name();
        }

    private:
        grpc::Status Configure(
            grpc::ServerContext* context,
            const generated::Configuration* request,
            google::protobuf::Empty* response) override;

        grpc::Status GetInformation(
            grpc::ServerContext* context,
            const google::protobuf::Empty* request,
            generated::Information* response) override;

        grpc::Status Shutdown(
            grpc::ServerContext* context,
            const google::protobuf::Empty* request,
            google::protobuf::Empty* response) override;
        
        EventLoop& m_eventLoop;
        rpc::RpcHandler& m_rpcHandler;
    };
}
