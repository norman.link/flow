#pragma once

#include <graph.grpc.pb.h>

namespace flow
{
    class NodeBase;
}

namespace flow::plugin
{
    class Plugin;
    class PluginRegistry;
}

namespace flow::rpc
{
    class RpcHandler;
}

namespace flow::services
{
    class Graph
        : public generated::Graph::Service
    {
    public:
        Graph(plugin::PluginRegistry& pluginRegistry, rpc::RpcHandler& rpcHandler);
        ~Graph();

        static constexpr char const* name()
        {
            return generated::Graph::service_full_name();
        }

        grpc::Status Start(
            grpc::ServerContext* context,
            const google::protobuf::Empty* request,
            google::protobuf::Empty* response) override;

        grpc::Status Stop(
            grpc::ServerContext* context,
            const google::protobuf::Empty* request,
            google::protobuf::Empty* response) override;

        grpc::Status Reset(
            grpc::ServerContext* context,
            const google::protobuf::Empty* request,
            google::protobuf::Empty* response) override;

        grpc::Status CreateNode(
            grpc::ServerContext* context,
            const generated::Node* request,
            google::protobuf::Empty* response) override;

        grpc::Status SetServiceChannel(
            grpc::ServerContext* context,
            const generated::ServiceChannel* request,
            google::protobuf::Empty* response) override;

        grpc::Status GetNodeProperties(
            grpc::ServerContext* context,
            const generated::Node* request,
            generated::NodeProperties* response) override;

        grpc::Status SetNodeProperty(
            grpc::ServerContext* context,
            const generated::NodeProperty* request,
            google::protobuf::Empty* response) override;
        
        grpc::Status ConnectNodes(
            grpc::ServerContext* context,
            const generated::NodeConnection* request,
            google::protobuf::Empty* response) override;

    private:
        enum class State
        {
            Stopped,
            Running
        };

        using NodeName = std::string;
        using NodeId = std::string;
        using SlotId = std::string;

        void stop();
        NodeBase& node(NodeId id) const;

        template <typename F>
        grpc::Status call(const F&& function) const
        {
            try
            {
                function();
                return grpc::Status(grpc::StatusCode::OK, "");
            }
            catch (const exceptions::ServiceException& e)
            {
                return e.status();
            }
            catch (const std::exception& e)
            {
                return grpc::Status(grpc::StatusCode::INTERNAL, e.what());
            }

            return grpc::Status(grpc::StatusCode::UNKNOWN, "unhandled exception");
        }

        mutable std::mutex m_mutex;
        plugin::PluginRegistry& m_pluginRegistry;
        State m_state;

        using AvailableNodesMap = std::unordered_map<NodeName, std::weak_ptr<plugin::Plugin>>;
        using CreatedNodesMap = std::unordered_map<NodeId, std::weak_ptr<plugin::Plugin>>;

        AvailableNodesMap m_availableNodes;
        CreatedNodesMap m_createdNodes;
        rpc::RpcHandler& m_rpcHandler;
    };
}
