#pragma once

#include <memory>
#include <grpc++/channel.h>

namespace flow
{
    class Application
    {
    public:
        Application(int port = -1);
        ~Application();

        int run();
        void shutdown();

        std::shared_ptr<grpc::Channel> inProcessChannel() const;

    private:
        void printInfo();

        struct Impl;
        std::unique_ptr<Impl> m_impl;
    };
}
