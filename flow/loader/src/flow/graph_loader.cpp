#include <flow/graph_loader.h>
#include <flow/application.h>
#include <application.grpc.pb.h>
#include <graph.grpc.pb.h>

#include <json/json.h>
#include <fstream>
#include <grpc++/channel.h>
#include <google/protobuf/util/json_util.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/any.pb.h>
#include <google/protobuf/dynamic_message.h>

namespace
{
    const flow::services::generated::Property* findPropertyValue(
        const std::string& name,
        const flow::services::generated::NodeProperties& properties)
    {
        for (int i = 0; i < properties.properties_size(); i++)
        {
            const auto& property = properties.properties(i);
            if (property.property().name() == name)
            {
                return &property.property();
            }
        }

        return nullptr;
    }

    std::string getPropertyName(const std::string& nodeId, const std::string& propertyName)
    {
        return nodeId + "." + propertyName;
    }
}

namespace flow
{
    struct GraphLoader::Impl
    {
        Impl(std::string graphFile)
            : m_graphFile{graphFile}
        {
        }

        std::unique_ptr<Application> load() const
        {
            using namespace google::protobuf;

            // load graph file
            Json::Value graph = load(m_graphFile);

            const auto& configuration = graph["configuration"];

            int port = -1;
            if (configuration.isMember("port"))
            {
                port = configuration["port"].asUInt();
            }

            // create application
            auto application = std::make_unique<Application>(port);
            auto channel = application->inProcessChannel();

            // create a stub using the supplied channel
            services::generated::Application::Stub applicationService(channel);
            services::generated::Graph::Stub graphService(channel);

            // shutdown graph before loading
            {
                grpc::ClientContext context;
                google::protobuf::Empty response;
                auto result = graphService.Stop(&context, Empty(), &response);
                if (!result.ok())
                {
                    throw std::runtime_error("could not stop graph: " + result.error_message());
                }
            }

            // reset all internal nodes
            {
                grpc::ClientContext context;
                Empty response;
                auto result = graphService.Reset(&context, Empty(), &response);
                if (!result.ok())
                {
                    throw std::runtime_error("could not reset graph: " + result.error_message());
                }
            }

            // configure graph
            setConfiguration(configuration, applicationService);
            createNodes(graph["nodes"], graphService);
            setProperties(graph["nodes"], graphService);
            createConnections(graph["connections"], graphService);

            // start graph
            {
                grpc::ClientContext context;
                Empty response;
                auto result = graphService.Start(&context, Empty(), &response);
                if (!result.ok())
                {
                    throw std::runtime_error("could not start graph: " + result.error_message());
                }
            }

            return std::move(application);
        }

    private:
        Json::Value load(const std::string& graphFile) const
        {
            std::ifstream graphFileStream(graphFile.c_str(), std::ifstream::binary);

            if (!graphFileStream.is_open())
            {
                throw std::runtime_error("Could not open graph file");
            }

            Json::Value graph;
            graphFileStream >> graph;

            return graph;
        }

        void setConfiguration(Json::Value configuration, services::generated::Application::Stub& applicationService) const
        {
            services::generated::Configuration request;

            if (configuration.isMember("channels"))
            {
                const auto& channels = configuration["channels"];
                for (const auto& channel : channels)
                {
                    auto newChannel = request.add_channels();
                    newChannel->set_name(channel["name"].asString());
                    newChannel->set_port(channel["port"].asInt());
                }
            }

            grpc::ClientContext context;
            google::protobuf::Empty response;
            const auto result = applicationService.Configure(&context, request, &response);

            if (!result.ok())
            {
                std::cerr << "could not configure: " << result.error_message() << std::endl;
            }
        }

        void createNodes(Json::Value nodes, services::generated::Graph::Stub& graphService) const
        {
            for (const auto& node : nodes)
            {
                grpc::ClientContext context;
                services::generated::Node request;
                request.set_name(node["name"].asString());
                request.set_id(node["id"].asString());
                google::protobuf::Empty response;
                const auto result = graphService.CreateNode(&context, request, &response);

                if (!result.ok())
                {
                    std::cerr << "could not create node: " << result.error_message() << std::endl;
                }

                if (node.isMember("channel"))
                {
                    grpc::ClientContext context;
                    services::generated::ServiceChannel request;
                    request.mutable_service()->set_id(node["id"].asString());
                    request.mutable_channel()->set_name(node["channel"].asString());
                    google::protobuf::Empty response;
                    const auto result = graphService.SetServiceChannel(&context, request, &response);

                    if (!result.ok())
                    {
                        std::cerr << "could not set service channel: " << result.error_message() << std::endl;
                    }
                }
            }
        }

        void setProperties(Json::Value nodes, services::generated::Graph::Stub& graphService) const
        {
            using namespace google::protobuf;

            for (const auto& node : nodes)
            {
                if (!node.isMember("properties"))
                {
                    continue;
                }

                const std::string& nodeId = node["id"].asString();

                // get available properties for this node
                services::generated::NodeProperties nodeProperties;
                {
                    grpc::ClientContext context;
                    services::generated::Node request;
                    request.set_id(nodeId);
                    const auto result = graphService.GetNodeProperties(&context, request, &nodeProperties);

                    if (!result.ok())
                    {
                        std::cerr << "could not create node: " << result.error_message() << std::endl;
                        continue;
                    }
                }

                const auto& properties = node["properties"];
                for (const auto& propertyName : properties.getMemberNames())
                {
                    const auto& propertyValue = findPropertyValue(propertyName, nodeProperties);
                    if (!propertyValue)
                    {
                        continue;
                    }

                    // generate a json representation for an any type
                    Json::Value jsonProperty = properties[propertyName];
                    std::string valueString = m_writer.write(jsonProperty);

                    services::generated::NodeProperty request;
                    {
                        // get full name from type url
                        std::string typeName;
                        internal::ParseAnyTypeUrl(propertyValue->value().type_url(), &typeName);

                        // create a descriptor database from the provided file descriptor
                        SimpleDescriptorDatabase database;
                        database.Add(propertyValue->proto());

                        // build a descriptor pool containing descriptors from this file
                        DescriptorPool pool(&database);
                        auto descriptor = pool.FindMessageTypeByName(typeName);
                        if (!descriptor)
                        {
                            std::cerr << "descriptor for type \"" << typeName << "\""
                                << " of property \"" << getPropertyName(nodeId, propertyName)
                                << "\" could not be found in pool" << std::endl;
                            continue;
                        }

                        // create a new dynamic message
                        DynamicMessageFactory factory(&pool);
                        auto value = factory.GetPrototype(descriptor)->New();

                        // parse into dynamic message
                        auto result = util::JsonStringToMessage(valueString, value);
                        if (!result.ok())
                        {
                            std::cerr << "could not parse property \""
                                << getPropertyName(nodeId, propertyName)
                                << "\" of type \"" << typeName << "\": "
                                << result.error_message().as_string() << std::endl;
                            delete value;
                            continue;
                        }

                        // convert to Any
                        request.mutable_property()->mutable_value()->PackFrom(*value);
                        delete value;
                    }

                    grpc::ClientContext context;
                    request.mutable_node()->set_id(nodeId);
                    request.mutable_property()->set_name(propertyName);

                    {
                        Empty response;
                        const auto result = graphService.SetNodeProperty(&context, request, &response);
                        if (!result.ok())
                        {
                            std::cerr << "could not set value of property \""
                                << getPropertyName(nodeId, propertyName)
                                << "\": " << result.error_message() << std::endl;
                        }
                    }
                }
            }
        }

        void createConnections(Json::Value connections, services::generated::Graph::Stub& graphService) const
        {
            for (const auto& connection : connections)
            {
                grpc::ClientContext context;
                services::generated::NodeConnection request;
                request.mutable_from()->mutable_node()->set_id(connection["from"].asString());
                request.mutable_from()->mutable_slot()->set_name(connection["output"].asString());
                request.mutable_to()->mutable_node()->set_id(connection["to"].asString());
                request.mutable_to()->mutable_slot()->set_name(connection["input"].asString());
                google::protobuf::Empty response;
                const auto result = graphService.ConnectNodes(&context, request, &response);

                if (!result.ok())
                {
                    std::cerr << "could not connect nodes: " << result.error_message() << std::endl;
                }
            }
        }

        mutable Json::FastWriter m_writer;
        std::string m_graphFile;
    };

    GraphLoader::GraphLoader(std::string graphFile)
    {
        m_impl = std::make_unique<GraphLoader::Impl>(std::move(graphFile));
    }

    GraphLoader::~GraphLoader()
    {
        // explicit destructor is necessary for pImpl with unique_ptr
    }

    std::unique_ptr<Application> GraphLoader::operator()() const
    {
        return std::move(m_impl->load());
    }
}
