#pragma once

#include <memory>
#include <string>

namespace flow
{
    class Application;

    class GraphLoader
    {
    public:
        GraphLoader(std::string graphFile);
        ~GraphLoader();

        std::unique_ptr<Application> operator()() const;

    private:
        struct Impl;
        std::unique_ptr<Impl> m_impl;
    };
}
