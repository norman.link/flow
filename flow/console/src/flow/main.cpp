#include <flow/application.h>

#ifdef HAS_JSONCPP
#include <flow/graph_loader.h>
#endif

#include <iostream>
#include <memory>

std::unique_ptr<flow::Application> application;

#ifdef _WIN32
#include <Windows.h> 
BOOL ConsoleCtrlHandler(DWORD ctrlType)
{ 
    if (ctrlType == CTRL_C_EVENT ||
        ctrlType == CTRL_CLOSE_EVENT)
    {
        try
        {
            application->shutdown();

            // the application might be killed too fast for the graph to free it's resources.
            Sleep(5000);
        }
        catch (...) {}

        return TRUE;
    }

    return FALSE;
}
#endif

int main(int argc, char** argv)
{
#ifdef _WIN32
    if (!SetConsoleCtrlHandler((PHANDLER_ROUTINE)ConsoleCtrlHandler, TRUE))
    {
        std::cerr << "could not set console control handler" << std::endl;
    }
#endif

    try
    {
#ifdef HAS_JSONCPP
        // load graph file
        if (argc > 1)
        {
            try
            {
                application = flow::GraphLoader(std::string(argv[1]))();
            }
            catch (const std::exception& e)
            {
                std::cerr << "could not load graph file: " << e.what() << std::endl;
            }
        }
#endif

        if (!application)
        {
            application = std::make_unique<flow::Application>();
        }

        // start event loop
        return application->run();
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}
