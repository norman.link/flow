[flow - a graph based application framework](https://gitlab.com/norman.link/flow)
===================================

# Documentation

* [Concepts](doc/Concepts.md)
* [Modules](doc/Modules.md)
* [Plugin structure](doc/PluginStructure.md)

# Installation & Testing

See [INSTALL](INSTALL.md) for installation instructions for various operating systems.

# Motivation

Building complex applications typically faces the same problems. There can be many different components interacting with each other, each with their own set of properties. They may depend on data, produce new data that are required by others or trigger actions. Truly reusable generic components are hard to build because they require an interface contract toward dependent components. Therefore, most application components used nowadays are not resuable out-of-the-box in different application contexts and require modifications.

# Overview

_flow_ is a multi-platform C++ application framework. It models software components as nodes in a graph. Nodes either produce, consume or transform data using a number of input and outputs slots of a specific data type. The framework loads a set of _plugin_ libraries (`.dll`s on windows, `.so` files on linux), which contain a set of node classes. Instances of nodes can then be created and connected at runtime with other nodes, possibly from different plugins and their properties can be changed dynamically. _flow_ is multithreaded using a dynamic thread pool and automatically takes care of dispatching implementation logic on the thread pool.

## Getting started

See the example implementations in the [examples](examples) folder.

## State

This framework is still work in progress and should not yet be used in production environments.

## License

This framework is licensed under the MIT license and can be used in both open-source and proprietary software.
