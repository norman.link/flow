#include "image_manipulation.h"

#include <iostream>

namespace
{
    flow::NodeRegistrar<ImageManipulation> registrar;
}

ImageManipulation::ImageManipulation()
{
}

grpc::Status ImageManipulation::mean(
    grpc::ServerContext* context,
    const types::Image* request,
    types::Image* response)
{
    std::cout << "received image: " << request->width() << " x " << request->height() << std::endl;
    response->set_width(400);
    response->set_height(500);
    return grpc::Status::OK;
}
