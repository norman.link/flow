#pragma once

#include <flow/service.h>
#include <manipulation.grpc.pb.h>

class ImageManipulation
    : public flow::Service<ImageManipulation, services::ImageManipulation>
{
public:
    ImageManipulation();

    static constexpr char const* name()
    {
        return "ImageManipulation";
    };

private:
    grpc::Status mean(
        grpc::ServerContext* context,
        const types::Image* request,
        types::Image* response) override;
};
