#include "generate.h"

#include <iostream>
#include <manipulation.grpc.pb.h>

namespace
{
    flow::NodeRegistrar<ImageGenerator> registrar;
}

ImageGenerator::ImageGenerator()
{
    addOutput<types::Image>("Image", generateImage());

    m_imageSize = addProperty<types::Size>("imageSize");
}

flow::OutputStream<types::Image> ImageGenerator::generateImage() const
{
    return rx::observable<>::create<flow::StreamData<types::Image>>(
        [this](rx::subscriber<flow::StreamData<types::Image>> subscriber)
        {
            while (subscriber.is_subscribed())
            {
                std::cout << "generating from " << id() << std::endl;

                subscriber.on_next(newImage());

                //std::this_thread::sleep_for(std::chrono::milliseconds(2));
            }
            std::cout << "generating done" << std::endl;
        }
    );
}

std::shared_ptr<types::Image> ImageGenerator::newImage() const
{
    auto image = std::make_shared<types::Image>();
    m_imageSize.query([image](const auto& size)
    {
        image->set_width(size.width());
        image->set_height(size.height());
    });
    return image;
}
