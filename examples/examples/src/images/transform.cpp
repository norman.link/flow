#include "transform.h"

#include <iostream>

namespace
{
    flow::NodeRegistrar<ImageTransformer> registrar;
}

ImageTransformer::ImageTransformer()
{
    auto input = addInput<types::Image>("Image")
        .map([this](flow::StreamData<types::Image> image)
        {
            std::cout << "went through " << id() << std::endl;
            return image;
        });

    addOutput<types::Image>("Image", input);
}
