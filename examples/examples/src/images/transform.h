#pragma once

#include <flow/node.h>
#include <definitions.pb.h>

class ImageTransformer
    : public flow::Node<ImageTransformer>
{
public:
    ImageTransformer();

    static constexpr char const* name()
    {
        return "ImageTransformer";
    };
};
