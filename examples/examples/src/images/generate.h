#pragma once

#include <flow/node.h>
#include <definitions.pb.h>

class ImageGenerator
    : public flow::Node<ImageGenerator>
{
public:
    ImageGenerator();

    static constexpr char const* name()
    {
        return "ImageGenerator";
    };

private:
    flow::OutputStream<types::Image> generateImage() const;
    std::shared_ptr<types::Image> newImage() const;

    flow::Property<types::Size> m_imageSize;
};
