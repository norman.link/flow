#include "consume.h"

#include <iostream>

namespace
{
    flow::NodeRegistrar<ImageConsumer> registrar;
}

ImageConsumer::ImageConsumer()
{
    processInput(addInput<types::Image>("Image"));
}

void ImageConsumer::processInput(flow::InputStream<types::Image> input)
{
    input.subscribe(
        [this](flow::StreamData<types::Image> image)
        {
            std::cout << "image received from " << id() << std::endl;

            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        },
        [this]()
        {
            std::cout << "complete from " << id() << std::endl;
        }
    );
}
