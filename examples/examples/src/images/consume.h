#pragma once

#include <flow/node.h>
#include <definitions.pb.h>

class ImageConsumer
    : public flow::Node<ImageConsumer>
{
public:
    ImageConsumer();

    static constexpr char const* name()
    {
        return "ImageConsumer";
    };

private:
    void processInput(flow::InputStream<types::Image> input);
};
