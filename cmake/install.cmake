configure_file(cmake/flow-config.cmake.in
    ${flow_INSTALL_CMAKEDIR}/flow-config.cmake @ONLY)

install(FILES
    cmake/generate_cpp.cmake
    cmake/plugin.cmake
    DESTINATION ${flow_INSTALL_CMAKEDIR})

configure_file(cmake/flow-config-version.cmake.in
    ${flow_INSTALL_CMAKEDIR}/flow-config-version.cmake @ONLY)
