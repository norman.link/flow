macro(flow_plugin PLUGIN_NAME)
    file(GLOB_RECURSE PROTO_FILES
        "${CMAKE_CURRENT_LIST_DIR}/proto/*.proto")

    file(GLOB_RECURSE SOURCES
        "${CMAKE_CURRENT_LIST_DIR}/src/*.cpp")

    list(FILTER SOURCES EXCLUDE REGEX ".*pch.cpp$")

    flow_generate_cpp(PROTO_SRCS PROTO_HDRS ${PROTO_FILES})

    set(SOURCES
        ${SOURCES}
        ${PROTO_SRCS}
        ${PROTO_HDRS})

    add_library(${PLUGIN_NAME} SHARED
        ${SOURCES})

    target_include_directories(${PLUGIN_NAME}
        PRIVATE src ${CMAKE_CURRENT_BINARY_DIR}/proto)

    target_compile_definitions(${PLUGIN_NAME}
        PRIVATE BUILD_DLL)

    target_link_libraries(${PLUGIN_NAME}
        flow::common)

    install(TARGETS ${PLUGIN_NAME}
        RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)

    install(DIRECTORY ${CMAKE_CURRENT_LIST_DIR}/data/
        DESTINATION ${CMAKE_INSTALL_PREFIX}/data/${PLUGIN_NAME})

    install(DIRECTORY ${CMAKE_CURRENT_LIST_DIR}/proto/
        DESTINATION ${CMAKE_INSTALL_PREFIX}/proto/${PLUGIN_NAME})
endmacro()
