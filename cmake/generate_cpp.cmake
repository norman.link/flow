function(flow_generate_cpp SRCS HDRS)
    cmake_parse_arguments(flow_generate_cpp "" "" "" ${ARGN})

    # _proto_files will be all arguments passed to this function
    set(_proto_files "${flow_generate_cpp_UNPARSED_ARGUMENTS}")
    if(NOT _proto_files)
        message(SEND_ERROR "Error: flow_generate_cpp() called without any proto files")
        return()
    endif()

    # it should always be possible to use proto files from protobuf itself
    set(flow_generate_cpp_IMPORT_DIRS ${PROTOBUF_INCLUDE_DIRS})

    # the proto/ directory will be the main include directory
    set(_protobuf_include_path -I ${CMAKE_CURRENT_SOURCE_DIR}/proto)
    foreach(DIR ${flow_generate_cpp_IMPORT_DIRS})
        get_filename_component(ABS_PATH ${DIR} ABSOLUTE)
        list(FIND _protobuf_include_path ${ABS_PATH} _contains_already)
        if(${_contains_already} EQUAL -1)
            list(APPEND _protobuf_include_path -I ${ABS_PATH})
        endif()
    endforeach()

    set(flow_generate_cpp_EXTENSIONS .pb.h .pb.cc .grpc.pb.h .grpc.pb.cc)
    set(_generated_srcs_all)
    foreach(_proto ${_proto_files})
        get_filename_component(_abs_file ${_proto} ABSOLUTE)
        get_filename_component(_basename ${_proto} NAME_WE)

        set(_generated_srcs)
        foreach(_ext ${flow_generate_cpp_EXTENSIONS})
            list(APPEND _generated_srcs "${CMAKE_CURRENT_BINARY_DIR}/proto/${_basename}${_ext}")
        endforeach()
        list(APPEND _generated_srcs_all ${_generated_srcs})

        add_custom_command(
            OUTPUT ${_generated_srcs}
            COMMAND protobuf::protoc
            ARGS
                --cpp_out ${CMAKE_CURRENT_BINARY_DIR}/proto
                --grpc_out ${CMAKE_CURRENT_BINARY_DIR}/proto --plugin=protoc-gen-grpc=$<TARGET_FILE:gRPC::grpc_cpp_plugin>
                ${_protobuf_include_path} ${_abs_file}
            DEPENDS ${ABS_FIL} protobuf::protoc gRPC::grpc_cpp_plugin
            COMMENT "Running protocol buffer compiler on ${_proto}"
        VERBATIM)
    endforeach()

    # mark the resulting generated files as GENERATED
    set_source_files_properties(${_generated_srcs_all} PROPERTIES GENERATED TRUE)

    # extract sources and headers
    set(${SRCS})
    set(${HDRS})
    foreach(_file ${_generated_srcs_all})
        if(_file MATCHES "cc$")
            list(APPEND ${SRCS} ${_file})
        else()
            list(APPEND ${HDRS} ${_file})
        endif()
    endforeach()

    # publish to the parent scope
    set(${SRCS} ${${SRCS}} PARENT_SCOPE)
    set(${HDRS} ${${HDRS}} PARENT_SCOPE)
endfunction()