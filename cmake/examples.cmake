include(ExternalProject)

function(add_examples_build NAME)
    get_filename_component(examples_dir examples/${NAME} ABSOLUTE)

    ExternalProject_Add(${NAME}
        PREFIX examples/${NAME}
        SOURCE_DIR "${examples_dir}"
        BINARY_DIR examples/${NAME}
        STAMP_DIR examples/${NAME}/logs
        INSTALL_COMMAND "" #Skip
        LOG_CONFIGURE 1
        CMAKE_CACHE_ARGS
        "-DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}"
        "-Dflow_DIR:PATH=${flow_INSTALL_CMAKEDIR}"
        "-DZLIB_LIBRARY:PATH=${ZLIB_LIBRARY}"
        "-DZLIB_INCLUDE_DIR:PATH=${ZLIB_INCLUDE_DIR}"
        "-DOPENSSL_ROOT_DIR:PATH=${OPENSSL_ROOT_DIR}"
        "-Dc-ares_DIR:PATH=${c-ares_DIR}")
    set_property(TARGET ${NAME} PROPERTY EXCLUDE_FROM_ALL TRUE)
endfunction()

add_examples_build(examples)
add_dependencies(examples common)
