# Building and testing

## Dependencies

* [Protobuf](https://github.com/google/protobuf)
* [gRPC](https://github.com/grpc/grpc)
* [RxCpp](https://github.com/Reactive-Extensions/RxCpp)
* [JsonCpp](https://github.com/open-source-parsers/jsoncpp)
* [GoogleTest](https://github.com/google/googletest)

JsonCpp is an optional dependency and only required for the `loader` library. It will load json graph definition files to initialize the graph.

## Infrastructure

flow uses the [Meson](http://mesonbuild.com) build system to generate build files and has a package recipe for the [Conan](https://conan.io) package manager. Is it recommended to use this workflow to build and use flow, but it can also be built without a conan package. Both workflows will be described here.

### Build requirements
* [PkgConfig](https://www.freedesktop.org/wiki/Software/pkg-config/)
* [Python 3.6](https://www.python.org/)
* Meson

On Windows, there is a lightweight pre-built binary available [here](https://sourceforge.net/projects/pkgconfiglite/files/).

Install meson with:
```bat
pip3 install meson
```

It is assumed that all dependencies listed above are available and built for the current platform and required build types.

### Building with Conan

* Install Conan

```bat
pip3 install conan
```

* Create the conan package
```bat
conan create . flow/stable
```

A specific platform `{arch}` and build type `{configuration}` can be created with
```bat
conan create . flow/stable -sarch={arch} -sbuild_type={configuration}
```
, where
* `{arch}` can be one of [`x86`, `x64`]
* `{configuration}` can be one of [`Debug`, `Release`]

### Building without Conan

TODO